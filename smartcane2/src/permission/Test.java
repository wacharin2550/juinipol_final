package permission;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import bus.TestManagement;
import connection.connectionMysql;




@WebServlet("/Test")
public class Test extends HttpServlet {
	private static final long serialVersionUID = 1L;
	connectionMysql connection = new connectionMysql();

    public Test() {
        super();
    }
    
    
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String method = request.getParameter("method");
		HttpSession session = request.getSession();
		HashMap<String, Object> respData = new HashMap<String, Object>();
		Gson gson = new Gson();
		String strJsonResp = "";
		System.out.println(method + " : method");
		String userName = (String)session.getAttribute("userName");
		String userID = (String)session.getAttribute("userId");
		Boolean statusID = (Boolean)session.getAttribute("statusOnline");
		try {
			if ("checkSession".equals(method)) {
				if (statusID!=null) {
					respData.put("respStatusOnline", true);
					respData.put("userNameID", userName);
					respData.put("userID", userID);
				}else {
					respData.put("respStatusOnline", false);
				}	
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		strJsonResp = gson.toJson(respData);
		response.setCharacterEncoding("UTF8");
		response.getWriter().print(strJsonResp);
	}



	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		TestManagement testManagement =   new TestManagement();
		String method = request.getParameter("method");
		HttpSession session = request.getSession();
		HashMap<String, Object> respData = new HashMap<String, Object>();
		Gson gson = new Gson();
		String strJsonResp = "";
		System.out.println(method + " : method");
		try {
			
			if ("login".equals(method)) {
				String pass = request.getParameter("pass");
				String user = request.getParameter("user");
				respData.put("status", testManagement.Login(connection.connection(), user, pass,session));
			}else if ("logout".equals(method)) {				
				session.removeAttribute("statusOnline");
				session.removeAttribute("userName");
				session.removeAttribute("userId");
				respData.put("status",true);
			} else {
				System.out.println("Not Method Post");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				connection.connection().close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		strJsonResp = gson.toJson(respData);
		response.setCharacterEncoding("UTF8");
		response.getWriter().print(strJsonResp);
	}

}
