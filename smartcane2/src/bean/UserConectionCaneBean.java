package bean;

public class UserConectionCaneBean {
	private String tokenID;
	private String caneName;
	private String caneId;
	private String caneUserId;
	private String caneStatus;
	private String smartCaneName;
	private String smartCaneToken;
	private String age;
	private String height;
	private String weight;
	
	public String getTokenID() {
		return tokenID;
	}
	public void setTokenID(String tokenID) {
		this.tokenID = tokenID;
	}
	public String getCaneName() {
		return caneName;
	}
	public void setCaneName(String caneName) {
		this.caneName = caneName;
	}
	public String getCaneId() {
		return caneId;
	}
	public void setCaneId(String caneId) {
		this.caneId = caneId;
	}
	public String getCaneUserId() {
		return caneUserId;
	}
	public void setCaneUserId(String caneUserId) {
		this.caneUserId = caneUserId;
	}
	public String getCaneStatus() {
		return caneStatus;
	}
	public void setCaneStatus(String caneStatus) {
		this.caneStatus = caneStatus;
	}
	public String getSmartCaneName() {
		return smartCaneName;
	}
	public void setSmartCaneName(String smartCaneName) {
		this.smartCaneName = smartCaneName;
	}
	public String getSmartCaneToken() {
		return smartCaneToken;
	}
	public void setSmartCaneToken(String smartCaneToken) {
		this.smartCaneToken = smartCaneToken;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getHeight() {
		return height;
	}
	public void setHeight(String height) {
		this.height = height;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	
	
}
