package bean;

public class UserLineSmartCaneBean {
	private String lineId;
	private String lineName;
	private String smartCaneLineStatusId;
	private String smartCaneName;
	public String getLineId() {
		return lineId;
	}
	public void setLineId(String lineId) {
		this.lineId = lineId;
	}
	public String getLineName() {
		return lineName;
	}
	public void setLineName(String lineName) {
		this.lineName = lineName;
	}
	public String getSmartCaneLineStatusId() {
		return smartCaneLineStatusId;
	}
	public void setSmartCaneLineStatusId(String smartCaneLineStatusId) {
		this.smartCaneLineStatusId = smartCaneLineStatusId;
	}
	public String getSmartCaneName() {
		return smartCaneName;
	}
	public void setSmartCaneName(String smartCaneName) {
		this.smartCaneName = smartCaneName;
	}
	
	
}
