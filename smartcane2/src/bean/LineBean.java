package bean;

public class LineBean {
	private String lineId;
	private String lineName;
	private String lineToken;
	private String lineStatus;
	public String getLineId() {
		return lineId;
	}
	public void setLineId(String lineId) {
		this.lineId = lineId;
	}
	public String getLineName() {
		return lineName;
	}
	public void setLineName(String lineName) {
		this.lineName = lineName;
	}
	public String getLineToken() {
		return lineToken;
	}
	public void setLineToken(String lineToken) {
		this.lineToken = lineToken;
	}
	public String getLineStatus() {
		return lineStatus;
	}
	public void setLineStatus(String lineStatus) {
		this.lineStatus = lineStatus;
	}
	

	
	
}
