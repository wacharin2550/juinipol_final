package bean;

public class LineCaneBean {
	private String lineName;
	private String caneName;
	private String lineCaneStatus;
	private String lineCaneId;
	private String caneUserId;
	
	public String getLineName() {
		return lineName;
	}
	public void setLineName(String lineName) {
		this.lineName = lineName;
	}
	public String getCaneName() {
		return caneName;
	}
	public void setCaneName(String caneName) {
		this.caneName = caneName;
	}

	public String getLineCaneStatus() {
		return lineCaneStatus;
	}
	public void setLineCaneStatus(String lineCaneStatus) {
		this.lineCaneStatus = lineCaneStatus;
	}
	public String getLineCaneId() {
		return lineCaneId;
	}
	public void setLineCaneId(String lineCaneId) {
		this.lineCaneId = lineCaneId;
	}
	public String getCaneUserId() {
		return caneUserId;
	}
	public void setCaneUserId(String caneUserId) {
		this.caneUserId = caneUserId;
	}
	
	
}
