package bean;

public class ReportUltrasonicBean {
	private String distanceMoreThanSixty;
	private String distanceMoreThanFive;
	private String distanceMoreThanThree;
	private String distanceTwentyNine;
	public String getDistanceMoreThanSixty() {
		return distanceMoreThanSixty;
	}
	public void setDistanceMoreThanSixty(String distanceMoreThanSixty) {
		this.distanceMoreThanSixty = distanceMoreThanSixty;
	}
	public String getDistanceMoreThanFive() {
		return distanceMoreThanFive;
	}
	public void setDistanceMoreThanFive(String distanceMoreThanFive) {
		this.distanceMoreThanFive = distanceMoreThanFive;
	}
	public String getDistanceMoreThanThree() {
		return distanceMoreThanThree;
	}
	public void setDistanceMoreThanThree(String distanceMoreThanThree) {
		this.distanceMoreThanThree = distanceMoreThanThree;
	}
	public String getDistanceTwentyNine() {
		return distanceTwentyNine;
	}
	public void setDistanceTwentyNine(String distanceTwentyNine) {
		this.distanceTwentyNine = distanceTwentyNine;
	}
	
	
}
