package bean;

public class LocationAreaBean {
	private String longitude;
	private String latitude;
	private String radius;
	private String caneId;
	private String locationAreaId;
	private String userIdForLine;
	private String caneUserId;
	private String locationStatus;
	
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getRadius() {
		return radius;
	}
	public void setRadius(String radius) {
		this.radius = radius;
	}
	public String getCaneId() {
		return caneId;
	}
	public void setCaneId(String caneId) {
		this.caneId = caneId;
	}
	public String getLocationAreaId() {
		return locationAreaId;
	}
	public void setLocationAreaId(String locationAreaId) {
		this.locationAreaId = locationAreaId;
	}
	public String getUserIdForLine() {
		return userIdForLine;
	}
	public void setUserIdForLine(String userIdForLine) {
		this.userIdForLine = userIdForLine;
	}
	public String getCaneUserId() {
		return caneUserId;
	}
	public void setCaneUserId(String caneUserId) {
		this.caneUserId = caneUserId;
	}
	public String getLocationStatus() {
		return locationStatus;
	}
	public void setLocationStatus(String locationStatus) {
		this.locationStatus = locationStatus;
	}

}
