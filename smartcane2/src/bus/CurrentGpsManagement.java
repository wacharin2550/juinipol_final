package bus;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.google.gson.Gson;

import bean.LocationAreaBean;
import bean.UserConectionCaneBean;

public class CurrentGpsManagement {
	
	public String getGpsLocation(Connection connection,int caneID,String userId) {
		PreparedStatement preparedStatement= null;
		String strResponse = "";
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();
		ArrayList<LocationAreaBean>alist = new ArrayList<LocationAreaBean>();
		try {

			sql.append(" SELECT LATITUDE,LONGITUDE "); 
			sql.append(" FROM GPS ");
			sql.append(" WHERE CANE_ID = ( SELECT CANE_ID FROM USER_CONECT_CANE ucc "); 
			sql.append(" WHERE ucc.USERS_ID = ? AND CANE_ID = ?) "); 
			sql.append(" ORDER BY GPS_ID DESC LIMIT 1 ");
			
			preparedStatement = connection.prepareStatement(sql.toString());
			preparedStatement.setString(1, userId);
			preparedStatement.setInt(2, caneID);
			rs = preparedStatement.executeQuery();
			while((rs!=null) && (rs.next())) {
				LocationAreaBean uBean = new LocationAreaBean();
				uBean.setLatitude(rs.getString("LATITUDE"));
				uBean.setLongitude(rs.getString("LONGITUDE"));
				alist.add(uBean);
			}
			strResponse = new Gson().toJson(alist);
		}catch (Exception e) {
			
		} finally {
			try {
				rs.close();
				preparedStatement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return strResponse;
	}
}
