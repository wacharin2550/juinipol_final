package bus;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.google.gson.Gson;

import bean.ReportUltrasonicBean;

public class ReportUltrasonicManagement {
	
	public String getUltrasonicHistory(Connection connection,int selectCane) {
		PreparedStatement preparedStatement= null;
		String strResponse = "";
		ResultSet rs = null;
		
		StringBuilder sql = new StringBuilder();
		ArrayList<ReportUltrasonicBean>alist = new ArrayList<ReportUltrasonicBean>();
		try {

			sql.append(" SELECT ULTRASONIC_ID, DISTANCE, CANE_ID "); 
			sql.append(" FROM ULTRASONIC ");
			sql.append(" WHERE CANE_ID = ? ");
			preparedStatement = connection.prepareStatement(sql.toString());
			preparedStatement.setInt(1, selectCane);
			rs = preparedStatement.executeQuery();
			while((rs!=null) && (rs.next())) {
				ReportUltrasonicBean uBean = new ReportUltrasonicBean();
				if (rs.getInt("DISTANCE")>60) {
					uBean.setDistanceMoreThanSixty(rs.getString("DISTANCE"));
				}else if (rs.getInt("DISTANCE")>50) {
					uBean.setDistanceMoreThanFive(rs.getString("DISTANCE"));
				}else if (rs.getInt("DISTANCE")>30) {
					uBean.setDistanceMoreThanThree(rs.getString("DISTANCE"));
				}else if (rs.getInt("DISTANCE")<=29) {
					uBean.setDistanceTwentyNine(rs.getString("DISTANCE"));
				}
				
				
				alist.add(uBean);
			}
			strResponse = new Gson().toJson(alist);
			System.out.println(strResponse);
		}catch (Exception e) {
			
		} finally {
			try {
				rs.close();
				preparedStatement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return strResponse;
	}
}
