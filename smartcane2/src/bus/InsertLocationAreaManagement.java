package bus;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.google.gson.Gson;

import bean.LocationAreaBean;
import bean.UserConectionCaneBean;

public class InsertLocationAreaManagement {
	
	public Boolean  insertLocationArea(Connection connection,String selectCane,String longitude,String latitude,String radiusArea,String checkBox) throws Exception{
		PreparedStatement stmt = null;
		StringBuilder strBuilder = new StringBuilder();
		Boolean doneInsert = false;
		int running = 1;
		try {
			if (checkRadius(connection,Integer.parseInt(selectCane))>0) {
				strBuilder.append(" UPDATE location_area ");		
				strBuilder.append(" SET LONGITUDE=?, LATITUDE=?, RADIUS_DISTANCE=? ");	
				if (Boolean.parseBoolean(checkBox) == true) {
					strBuilder.append(" , LOCATION_STATUS = 1 ");	
				}else {
					strBuilder.append(" , LOCATION_STATUS = 0 ");	
				}
				strBuilder.append(" WHERE CANE_USER_ID  = ?; ");	
				stmt = connection.prepareStatement(strBuilder.toString());		
				stmt.setString(running++, longitude);
				stmt.setString(running++, latitude);
				stmt.setString(running++, radiusArea);
				stmt.setString(running++, selectCane);
				int insertedTotal = stmt.executeUpdate();
				System.out.println(insertedTotal+" : UPDATE");
				if (insertedTotal>0) {
					doneInsert = true;
				} else {
					doneInsert = false;
				}
			}else {
				strBuilder.append(" INSERT INTO LOCATION_AREA ");		
				strBuilder.append(" (CANE_USER_ID, LONGITUDE, LATITUDE, RADIUS_DISTANCE, LOCATION_STATUS) ");		
				strBuilder.append(" VALUES(?, ?, ?, ?, ?); ");		
				stmt = connection.prepareStatement(strBuilder.toString());		
				stmt.setString(1, selectCane);
				stmt.setString(2, longitude);
				stmt.setString(3, latitude);
				stmt.setString(4, radiusArea);
			
				int insertedTotal = stmt.executeUpdate();
				System.out.println(insertedTotal+" : insertedTotal");
				if (insertedTotal>0) {
					doneInsert = true;
				} else {
					doneInsert = false;
				}
			}
			
			
		}  catch (Exception e) {
			e.printStackTrace();
		} finally {
			connection.close();
			stmt.close();
		}
		return doneInsert;
	}
	
	public String getSmartCaneRadius(Connection connection,int userCaneId) {
		PreparedStatement preparedStatement= null;
		String strResponse = "";
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();
		ArrayList<LocationAreaBean>alist = new ArrayList<LocationAreaBean>();
		try {
			sql.append(" SELECT LOCATION_AREA_ID, la.CANE_USER_ID, LONGITUDE, LATITUDE,  "); 
			sql.append(" RADIUS_DISTANCE, LOCATION_STATUS ");
			sql.append(" FROM LOCATION_AREA la "); 
			sql.append(" LEFT JOIN USER_CONECT_CANE ucc ON ucc.CANE_USER_ID = la.CANE_USER_ID "); 
			sql.append(" WHERE ucc.CANE_USER_ID = ?");
			preparedStatement = connection.prepareStatement(sql.toString());
			preparedStatement.setInt(1, userCaneId);
			rs = preparedStatement.executeQuery();
			while((rs!=null) && (rs.next())) {
				LocationAreaBean uBean = new LocationAreaBean();
				uBean.setRadius(rs.getString("RADIUS_DISTANCE"));
				uBean.setLatitude(rs.getString("LATITUDE"));
				uBean.setLongitude(rs.getString("LONGITUDE"));
				uBean.setLocationStatus(rs.getString("LOCATION_STATUS"));
				alist.add(uBean);
			}
			strResponse = new Gson().toJson(alist);
		}catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
				rs.close();
				preparedStatement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return strResponse;
	}
	
	public int checkRadius(Connection connection,int userCaneId) {
		PreparedStatement preparedStatement= null;
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();
		int total = 0;
		try {
			sql.append(" SELECT LOCATION_AREA_ID, la.CANE_USER_ID, LONGITUDE, LATITUDE,  "); 
			sql.append(" RADIUS_DISTANCE, LOCATION_STATUS ");
			sql.append(" FROM LOCATION_AREA la "); 
			sql.append(" LEFT JOIN USER_CONECT_CANE ucc ON ucc.CANE_USER_ID = la.CANE_USER_ID "); 
			sql.append(" WHERE ucc.CANE_USER_ID = ?");
			preparedStatement = connection.prepareStatement(sql.toString());
			preparedStatement.setInt(1, userCaneId);
			rs = preparedStatement.executeQuery();
			while((rs!=null) && (rs.next())) {
				total++;
			}
		}catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				preparedStatement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return total;
	}
}
