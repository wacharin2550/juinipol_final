package bus;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.google.gson.Gson;

import bean.ReportEmergencyBean;

public class ReportEmergencyManagement {

	public String getEmergencyHistory(Connection connection, int selectCane, String yearSearch,String monthSeach,String yearSearchForMonth) {
		PreparedStatement preparedStatement = null;
		String strResponse = "";
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();
		ArrayList<ReportEmergencyBean> alist = new ArrayList<ReportEmergencyBean>();
		
		try {
			sql.append(" SELECT  MONTH(CREATE_DATE) AS CAL_MONTH , ");
			sql.append(" (case when CREATE_DATE between ? and ? then DAY(CREATE_DATE) else '' end) as DAYS ");
			sql.append(" FROM EMERGENCY ");
			sql.append(" WHERE CREATE_DATE between ? and ? ");
			sql.append(" AND CANE_ID = ? ");
			preparedStatement = connection.prepareStatement(sql.toString());
			preparedStatement.setString(1, yearSearchForMonth.concat("-").concat(monthSeach).concat("-01 00:00:00"));
			preparedStatement.setString(2, yearSearchForMonth.concat("-").concat(monthSeach).concat("-31 23:59:59"));
			preparedStatement.setString(3, yearSearch.concat("-01-01 00:00:00"));
			preparedStatement.setString(4, yearSearch.concat("-12-31 23:59:59"));
			preparedStatement.setInt(5, selectCane);
			rs = preparedStatement.executeQuery();
			while ((rs != null) && (rs.next())) {
				ReportEmergencyBean uBean = new ReportEmergencyBean();
				uBean.setMonth(rs.getString("CAL_MONTH"));
				uBean.setDay(rs.getString("DAYS"));
				alist.add(uBean);
			}
			strResponse = new Gson().toJson(alist);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
				rs.close();
				preparedStatement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return strResponse;
	}
	
	public String getEmergencyChooseHistory(Connection connection, int selectCane, String searchStartDate,String searchEndDate) {
		PreparedStatement preparedStatement = null;
		String strResponse = "";
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();
		ArrayList<ReportEmergencyBean> alist = new ArrayList<ReportEmergencyBean>();
		
		try {
			sql.append(" SELECT CREATE_DATE , LATITUDE, LONGITUDE ");
			sql.append(" FROM EMERGENCY ");
			sql.append(" WHERE CREATE_DATE between ? and ? ");
			sql.append(" AND CANE_ID = ? ");
			sql.append(" order by CREATE_DATE asc ");
			preparedStatement = connection.prepareStatement(sql.toString());
			preparedStatement.setString(1, searchStartDate.concat(" 00:00:00"));
			preparedStatement.setString(2, searchEndDate.concat(" 23:59:59"));
			preparedStatement.setInt(3, selectCane);
			rs = preparedStatement.executeQuery();
			while ((rs != null) && (rs.next())) {
				ReportEmergencyBean uBean = new ReportEmergencyBean();
				uBean.setCreateDate(rs.getString("CREATE_DATE"));
				uBean.setLatitude(rs.getString("LATITUDE"));
				uBean.setLongitude(rs.getString("LONGITUDE"));
				alist.add(uBean);
			}
			strResponse = new Gson().toJson(alist);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
				rs.close();
				preparedStatement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return strResponse;
	}
}
