package bus;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.google.gson.Gson;

import bean.LocationAreaBean;
import servlet.LineNotify;

public class InsertGpsManagement {
	
	public Boolean  insertGps(Connection connection,String caneToken,String Latitude,String Longitude) throws Exception{
		PreparedStatement stmt = null;
		StringBuilder strBuilder = new StringBuilder();
		int gpsID = 0; 
		Boolean success = false;
		try {
			strBuilder.append(" INSERT INTO GPS ");		
			strBuilder.append(" (CANE_ID, LONGITUDE, LATITUDE) ");		
			strBuilder.append(" VALUES((SELECT CANE_ID FROM CANE c WHERE c.CANE_TOKEN = ?), ?, ?); ");		
			stmt = connection.prepareStatement(strBuilder.toString(),PreparedStatement.RETURN_GENERATED_KEYS);	
			stmt.setString(1, caneToken);
			stmt.setString(2, Longitude);
			stmt.setString(3, Latitude);
			
			int insertedTotal = stmt.executeUpdate();
			ResultSet resultSet = stmt.getGeneratedKeys();
			if (resultSet != null && resultSet.next()) {
				gpsID = resultSet.getInt(1);
			}
			if (insertedTotal>0&&gpsID!=0) {		
				if (insertGpsHistory(connection,gpsID)>0) {
					success = true;
					System.out.println("-- insert Gps Done --"+insertedTotal+"row");
				}else {
					success = false;
					System.out.println("-- insert Gps Fail --");
				}		
			} else {
				success = false;
				System.out.println("-- insert Gps Fail --");
			}
			
			resultSet.close();
		}  catch (Exception e) {
			e.printStackTrace();
		} finally {
			connection.close();
			stmt.close();
		}
		return success;
	}
	public int  insertGpsHistory(Connection connection,int gpsID) throws Exception{
		PreparedStatement stmt = null;
		StringBuilder strBuilder = new StringBuilder();
		int insertedTotal = 0;
		try {
			strBuilder.append(" INSERT INTO GPS_HISTORY ");		
			strBuilder.append(" (GPS_ID, CREATE_DATE) ");		
			strBuilder.append(" VALUES(?, NOW()); ");		
			stmt = connection.prepareStatement(strBuilder.toString());	
			stmt.setInt(1, gpsID);
			insertedTotal = stmt.executeUpdate();
			
			if (insertedTotal>0) {	
				System.out.println("-- INSERT insert GpsHistory Done -- "+insertedTotal+" Row");
			} else {
				System.out.println("-- INSERT insert GpsHistory Fail --");
			}
		}  catch (Exception e) {
			e.printStackTrace();
		} finally {
			connection.close();
			stmt.close();
		}
		return insertedTotal;
	}
	
	public String getLocationArea(Connection connection,String caneToken) {
		PreparedStatement preparedStatement = null;
		String strResponse = "";
		ResultSet rs = null;

		StringBuilder sql = new StringBuilder();
		ArrayList<LocationAreaBean> alist = new ArrayList<LocationAreaBean>();
		try {

			sql.append(" SELECT LOCATION_AREA_ID,ucc.USERS_ID,la.CANE_USER_ID,ucc.CANE_ID,  LONGITUDE, LATITUDE, ");
			sql.append(" RADIUS_DISTANCE, LOCATION_STATUS ");
			sql.append(" FROM LOCATION_AREA la ");
			sql.append(" LEFT JOIN USER_CONECT_CANE ucc ON ucc.CANE_USER_ID = la.CANE_USER_ID ");
			sql.append(" WHERE ucc.CANE_ID = (SELECT CANE_ID FROM CANE WHERE CANE_TOKEN = ?)");
			sql.append(" AND la.LOCATION_STATUS = 1 ");
			preparedStatement = connection.prepareStatement(sql.toString());
			preparedStatement.setString(1, caneToken);
			rs = preparedStatement.executeQuery();
			while ((rs != null) && (rs.next())) {
				LocationAreaBean uBean = new LocationAreaBean();
				uBean.setLongitude(rs.getString("LONGITUDE"));
				uBean.setLatitude(rs.getString("LATITUDE"));
				uBean.setLocationAreaId(rs.getString("LOCATION_AREA_ID"));
				uBean.setUserIdForLine(rs.getString("USERS_ID"));
				uBean.setCaneId(rs.getString("CANE_ID"));
				uBean.setCaneUserId(rs.getString("CANE_USER_ID"));
				uBean.setRadius(rs.getString("RADIUS_DISTANCE"));
				alist.add(uBean);
			}
			strResponse = new Gson().toJson(alist);
		} catch (Exception e) {

		} finally {
			try {
				connection.close();
				rs.close();
				preparedStatement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return strResponse;
	}
	public void sendToLindRadius(Connection connection,String caneId,String userIdForLine,String latitude,String longitude
			,String locationAreaId) {
		PreparedStatement preparedStatement= null;
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();
		LineNotify ln ;
		int totalCall = 0;
		try {

			sql.append(" SELECT LINE_CONECTION_CANE,ucc.USERS_ID, ucc.CANE_NAME,c.CANE_TOKEN, l.LINE_NAME ,l.LINE_TOKEN "); 
			sql.append(" FROM LINE_CONECTION_CANE lcc ");
			sql.append(" LEFT JOIN USER_CONECT_CANE ucc ON ucc.CANE_USER_ID = lcc.CANE_USER_ID "); 
			sql.append(" LEFT JOIN LINE l ON l.LINE_ID = lcc.LINE_ID "); 
			sql.append(" LEFT JOIN CANE c ON c.CANE_ID = ucc.CANE_ID "); 
			sql.append(" WHERE c.CANE_ID = ? AND lcc.LINE_STATUS = 1 ");
			sql.append(" AND l.USERS_ID = ? ");
			
			preparedStatement = connection.prepareStatement(sql.toString());
			preparedStatement.setString(1, caneId);
			preparedStatement.setString(2, userIdForLine);
			rs = preparedStatement.executeQuery();
					
			while((rs!=null) && (rs.next())) {
				ln = new LineNotify(rs.getString("LINE_TOKEN"));
				ln.notifyMe("Radius Alert  Cane Name :"+ rs.getString("CANE_NAME")+" : https://www.google.com/maps/search/"+latitude+","+longitude+"/19z");
				totalCall++;
			}
			if (totalCall>0) {
				insertLocationAlert(connection, latitude,longitude,  locationAreaId);
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
				rs.close();
				preparedStatement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void  insertLocationAlert(Connection connection,String latitude,String longitude,String locationAreaId) throws Exception{
		PreparedStatement stmt = null;
		StringBuilder strBuilder = new StringBuilder();
		
		try {
			strBuilder.append(" INSERT INTO LOCATION_AREA_HISTORY ");		
			strBuilder.append(" (LOCATION_AREA_ID, LONGITUDE, LATITUDE, CREATE_DATE) ");		
			strBuilder.append(" VALUES(?, ?, ?, NOW()); ");		
			stmt = connection.prepareStatement(strBuilder.toString());	
			stmt.setString(1, locationAreaId);
			stmt.setString(2, longitude);
			stmt.setString(3, latitude);
			
			int insertedTotal = stmt.executeUpdate();
			if (insertedTotal>0) {	
				System.out.println("-- Insert LOCATION_AREA_HISTORY  Success --");
			} else {
				System.out.println("-- Insert LOCATION_AREA_HISTORY  Fail --");
			}
			
		}  catch (Exception e) {
			e.printStackTrace();
		} finally {
			connection.close();
			stmt.close();
		}
	}
}
