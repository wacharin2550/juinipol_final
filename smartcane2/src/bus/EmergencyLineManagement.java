package bus;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.google.gson.Gson;

import bean.LineCaneBean;
import bean.LocationAreaBean;
import servlet.LineNotify;

public class EmergencyLineManagement {
	
	public void  insertEmergency(Connection connection,String caneToken,String latitude,String longitude) throws Exception{
		PreparedStatement stmt = null;
		StringBuilder strBuilder = new StringBuilder();
		
		try {
			
			strBuilder.append(" INSERT INTO EMERGENCY ");		
			strBuilder.append(" (CANE_ID, LONGITUDE, LATITUDE, CREATE_DATE) ");		
			strBuilder.append(" VALUES((SELECT CANE_ID FROM CANE c WHERE c.CANE_TOKEN=?), ?, ?, NOW()); ");		
			stmt = connection.prepareStatement(strBuilder.toString());	
			stmt.setString(1, caneToken);
			stmt.setString(2, longitude);
			stmt.setString(3, latitude);
			
			int insertedTotal = stmt.executeUpdate();
			if (insertedTotal>0) {	
				System.out.println("-- Insert Emergency  Success --");
			} else {
				System.out.println("-- Insert Emergency  Fail --");
			}
			
			
		}  catch (Exception e) {
			e.printStackTrace();
		} finally {
			connection.close();
			stmt.close();
		}
	}
	
	public void sendToLindEmergency(Connection connection,String caneToken,String userId,String latitude,String longitude) {
		PreparedStatement preparedStatement= null;
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();
		LineNotify ln ;
		int totalCall = 0;
		try {

			sql.append(" SELECT LINE_CONECTION_CANE, ucc.CANE_NAME,c.CANE_TOKEN, l.LINE_NAME ,l.LINE_TOKEN "); 
			sql.append(" FROM LINE_CONECTION_CANE lcc ");
			sql.append(" LEFT JOIN USER_CONECT_CANE ucc ON ucc.CANE_USER_ID = lcc.CANE_USER_ID "); 
			sql.append(" LEFT JOIN LINE l ON l.LINE_ID = lcc.LINE_ID "); 
			sql.append(" LEFT JOIN CANE c ON c.CANE_ID = ucc.CANE_ID  "); 
			sql.append(" WHERE c.CANE_TOKEN = ? AND lcc.LINE_STATUS = 1 ");
			
			preparedStatement = connection.prepareStatement(sql.toString());
			preparedStatement.setString(1, caneToken);
			rs = preparedStatement.executeQuery();
					
			while((rs!=null) && (rs.next())) {
				ln = new LineNotify(rs.getString("LINE_TOKEN"));
				ln.notifyMe("Emergency Alert Cane Name : "+ rs.getString("CANE_NAME")+" : https://www.google.com/maps/search/"+latitude+","+longitude+"/19z");
				totalCall++;
				System.out.println("Emergency Alert ");
			}
			if (totalCall>0) {
				insertEmergency(connection, caneToken, latitude, longitude);
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
				rs.close();
				preparedStatement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
}
