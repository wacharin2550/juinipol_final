package bus;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.google.gson.Gson;

import bean.ReportLocationAlertBean;

public class ReportLocationAlertManagement {

	public String getLocationAlertHistory(Connection connection,String userId, int selectCane, String yearSearch,String monthSeach,String yearSearchForMonth) {
		PreparedStatement preparedStatement = null;
		String strResponse = "";
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();
		ArrayList<ReportLocationAlertBean> alist = new ArrayList<ReportLocationAlertBean>();
		try {
			sql.append(" SELECT  MONTH(CREATE_DATE) AS CAL_MONTH , ");
			sql.append(" (case when CREATE_DATE between ? and ? then DAY(CREATE_DATE) else '' end) as DAYS ");
			sql.append(" FROM LOCATION_AREA_HISTORY lah ");
			sql.append(" LEFT JOIN LOCATION_AREA la ON la.LOCATION_AREA_ID = lah.LOCATION_AREA_ID ");
			sql.append(" LEFT JOIN USER_CONECT_CANE ucc ON ucc.CANE_USER_ID = la.CANE_USER_ID ");
			sql.append(" WHERE ucc.USERS_ID = ? AND CREATE_DATE between ? and ? ");
			sql.append(" AND la.CANE_USER_ID = ? ");
			preparedStatement = connection.prepareStatement(sql.toString());
			preparedStatement.setString(1, yearSearchForMonth.concat("-").concat(monthSeach).concat("-01 00:00:00"));
			preparedStatement.setString(2, yearSearchForMonth.concat("-").concat(monthSeach).concat("-31 23:59:59"));
			preparedStatement.setString(3, userId);
			preparedStatement.setString(4, yearSearch.concat("-01-01 00:00:00"));
			preparedStatement.setString(5, yearSearch.concat("-12-31 23:59:59"));
			preparedStatement.setInt(6, selectCane);
			rs = preparedStatement.executeQuery();
			while ((rs != null) && (rs.next())) {
				ReportLocationAlertBean uBean = new ReportLocationAlertBean();
				uBean.setMonth(rs.getString("CAL_MONTH"));
				uBean.setDay(rs.getString("DAYS"));
				alist.add(uBean);
			}
			strResponse = new Gson().toJson(alist);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
				rs.close();
				preparedStatement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return strResponse;
	}
	
	public String getChooseLocationAlertHistory(Connection connection,int selectCane,String userId, String searchStartDate,String searchEndDate) {
		PreparedStatement preparedStatement = null;
		String strResponse = "";
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();
		ArrayList<ReportLocationAlertBean> alist = new ArrayList<ReportLocationAlertBean>();
		try {
			sql.append(" SELECT lah.CREATE_DATE ,lah.LATITUDE,lah.LONGITUDE ");
			sql.append(" FROM LOCATION_AREA_HISTORY lah ");
			sql.append(" LEFT JOIN LOCATION_AREA la ON la.LOCATION_AREA_ID = lah.LOCATION_AREA_ID ");
			sql.append(" LEFT JOIN USER_CONECT_CANE ucc ON ucc.CANE_USER_ID = la.CANE_USER_ID ");
			sql.append(" WHERE ucc.USERS_ID = ? AND CREATE_DATE between ? and ? ");
			sql.append(" AND la.CANE_USER_ID = ? ");
			sql.append(" order by CREATE_DATE asc ");
			preparedStatement = connection.prepareStatement(sql.toString());
			preparedStatement.setString(1, userId);
			preparedStatement.setString(2, searchStartDate.concat(" 00:00:00"));
			preparedStatement.setString(3, searchEndDate.concat(" 23:59:59"));
			preparedStatement.setInt(4, selectCane);
			rs = preparedStatement.executeQuery();
			while ((rs != null) && (rs.next())) {
				ReportLocationAlertBean uBean = new ReportLocationAlertBean();
				uBean.setCreateDate(rs.getString("CREATE_DATE"));
				uBean.setLatitude(rs.getString("LATITUDE"));
				uBean.setLongitude(rs.getString("LONGITUDE"));
				alist.add(uBean);
			}
			strResponse = new Gson().toJson(alist);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
				rs.close();
				preparedStatement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return strResponse;
	}
}
