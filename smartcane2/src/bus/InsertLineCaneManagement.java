package bus;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.google.gson.Gson;

import bean.LineBean;
import bean.LineCaneBean;

public class InsertLineCaneManagement {
	
	public String getLineCaneName(Connection connection, int userId) {
		PreparedStatement preparedStatement = null;
		String strResponse = "";
		ResultSet rs = null;

		StringBuilder sql = new StringBuilder();
		ArrayList<LineCaneBean> alist = new ArrayList<LineCaneBean>();
		try {

			sql.append(" SELECT LINE_CONECTION_CANE,lcc.CANE_USER_ID , ucc.CANE_NAME,c.CANE_TOKEN, l.LINE_NAME ,l.LINE_TOKEN,lcc.LINE_STATUS ");
			sql.append(" FROM LINE_CONECTION_CANE lcc ");
			sql.append(" LEFT JOIN USER_CONECT_CANE ucc ON ucc.CANE_USER_ID = lcc.CANE_USER_ID ");
			sql.append(" LEFT JOIN LINE l ON l.LINE_ID = lcc.LINE_ID ");
			sql.append(" LEFT JOIN CANE c ON c.CANE_ID = ucc.CANE_ID ");
			sql.append(" LEFT JOIN LOCATION_AREA la ON la.CANE_USER_ID = ucc.CANE_USER_ID ");
			sql.append(" WHERE l.USERS_ID = ? ");
			preparedStatement = connection.prepareStatement(sql.toString());
			preparedStatement.setInt(1, userId);
			rs = preparedStatement.executeQuery();
			while ((rs != null) && (rs.next())) {
				LineCaneBean uBean = new LineCaneBean();
				uBean.setLineName(rs.getString("LINE_NAME"));
				uBean.setCaneName(rs.getString("CANE_NAME"));
				uBean.setLineCaneId(rs.getString("LINE_CONECTION_CANE"));
				uBean.setLineCaneStatus(rs.getString("LINE_STATUS"));
				uBean.setCaneUserId(rs.getString("CANE_USER_ID"));

				alist.add(uBean);
			}
			strResponse = new Gson().toJson(alist);
		} catch (Exception e) {

		} finally {
			try {
				connection.close();
				rs.close();
				preparedStatement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return strResponse;
	}

	public int insertLineCane(Connection connection, int userId, LineCaneBean[] lineCaneName)throws Exception {
		PreparedStatement stmt = null;
		StringBuilder strBuilder = new StringBuilder();
		int insertedTotal = 0;
		try {
			strBuilder.append(" INSERT LINE_CONECTION_CANE ");
			strBuilder.append(" (CANE_USER_ID, LINE_ID, LINE_STATUS) ");
			strBuilder.append(" VALUES( (SELECT CANE_USER_ID  FROM USER_CONECT_CANE WHERE CANE_NAME = ? AND USERS_ID = ?), (SELECT LINE_ID  FROM LINE WHERE LINE_NAME = ? AND USERS_ID = ?), 1); ");
			stmt = connection.prepareStatement(strBuilder.toString());
			for (LineCaneBean bean : lineCaneName) {
				stmt.setString(1, bean.getCaneName());
				stmt.setInt(2, userId);
				stmt.setString(3, bean.getLineName());
				stmt.setInt(4, userId);
				insertedTotal = stmt.executeUpdate();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			connection.close();
		}
		return insertedTotal;
	}
	
	public int updateLineCane(Connection connection, String lineCaneId)throws Exception {
		PreparedStatement stmt = null;
		StringBuilder strBuilder = new StringBuilder();
		int insertedTotal = 0;
		try {
			strBuilder.append(" UPDATE LINE_CONECTION_CANE ");
			strBuilder.append(" SET  LINE_STATUS = 0 ");
			strBuilder.append(" WHERE LINE_CONECTION_CANE  = ? ; ");
			stmt = connection.prepareStatement(strBuilder.toString());
			stmt.setString(1, lineCaneId);
			insertedTotal = stmt.executeUpdate();
		
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			connection.close();
		}
		return insertedTotal;
	}
	public int updateStartLineCane(Connection connection, String lineCaneId)throws Exception {
		PreparedStatement stmt = null;
		StringBuilder strBuilder = new StringBuilder();
		int insertedTotal = 0;
		try {
			strBuilder.append(" UPDATE LINE_CONECTION_CANE ");
			strBuilder.append(" SET  LINE_STATUS = 1 ");
			strBuilder.append(" WHERE LINE_CONECTION_CANE  = ? ; ");
			stmt = connection.prepareStatement(strBuilder.toString());
			stmt.setString(1, lineCaneId);
			insertedTotal = stmt.executeUpdate();
		
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			connection.close();
		}
		return insertedTotal;
	}
}
