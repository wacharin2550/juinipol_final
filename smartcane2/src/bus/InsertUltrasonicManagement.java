package bus;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class InsertUltrasonicManagement {
	
	public void  insertUltrasonic(Connection connection,String caneToken,String distance) throws Exception{
		PreparedStatement stmt = null;
		StringBuilder strBuilder = new StringBuilder();
		try {
			strBuilder.append(" INSERT INTO ULTRASONIC ");		
			strBuilder.append(" (DISTANCE, CANE_ID) ");		
			strBuilder.append(" VALUES(?, (SELECT CANE_ID FROM CANE c WHERE c.CANE_TOKEN = ?)); ");		
			stmt = connection.prepareStatement(strBuilder.toString());		
			stmt.setString(1, distance);
			stmt.setString(2, caneToken);
			stmt.executeUpdate();
			insertUltrasonicHistory(connection);
			
		}  catch (Exception e) {
			e.printStackTrace();
		} finally {
			connection.close();
			stmt.close();
		}
	}
	
	public void  insertUltrasonicHistory(Connection connection) throws Exception{
		PreparedStatement stmt = null;
		StringBuilder strBuilder = new StringBuilder();
		try {
			strBuilder.append(" INSERT INTO ULTRASONIC_HISTORY ");		
			strBuilder.append(" (ULTRASONIC_ID, CREATE_DATE) ");		
			strBuilder.append(" VALUES((SELECT ULTRASONIC_ID FROM ULTRASONIC ORDER BY ULTRASONIC_ID DESC LIMIT 1), NOW()); ");		
			stmt = connection.prepareStatement(strBuilder.toString());		
			stmt.executeUpdate();
			
		}  catch (Exception e) {
			e.printStackTrace();
		} finally {
			connection.close();
			stmt.close();
		}
	}
}
