package bus;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.google.gson.Gson;

import bean.ReportEmergencyBean;
import bean.ReportGpsBean;

public class ReportGpsManagement {
	
	public String getGpsChooseHistory(Connection connection, int selectCane, String searchStartDate,String searchEndDate) {
		PreparedStatement preparedStatement = null;
		String strResponse = "";
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();
		ArrayList<ReportGpsBean> alist = new ArrayList<ReportGpsBean>();
		
		try {
			sql.append(" SELECT CREATE_DATE , LATITUDE, LONGITUDE ");
			sql.append(" FROM gps g ");
			sql.append(" left join gps_history gh on gh.GPS_ID = g.GPS_ID ");
			sql.append(" WHERE CREATE_DATE between ? and ? ");
			sql.append(" AND CANE_ID = ? ");
			sql.append(" order by CREATE_DATE asc ");
			preparedStatement = connection.prepareStatement(sql.toString());
			preparedStatement.setString(1, searchStartDate.concat(" 00:00:00"));
			preparedStatement.setString(2, searchEndDate.concat(" 23:59:59"));
			preparedStatement.setInt(3, selectCane);
			rs = preparedStatement.executeQuery();
			while ((rs != null) && (rs.next())) {
				ReportGpsBean uBean = new ReportGpsBean();
				uBean.setCreateDate(rs.getString("CREATE_DATE"));
				uBean.setLatitude(rs.getString("LATITUDE"));
				uBean.setLongitude(rs.getString("LONGITUDE"));
				alist.add(uBean);
			}
			strResponse = new Gson().toJson(alist);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
				rs.close();
				preparedStatement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return strResponse;
	}
}
