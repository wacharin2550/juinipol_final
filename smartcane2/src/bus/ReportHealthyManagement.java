package bus;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import com.google.gson.Gson;

import bean.GpsBean;
import bean.ReportEmergencyBean;
import bean.ReportHealthyBean;

public class ReportHealthyManagement {

	public String getReportHealthy(Connection connection, int selectCane, String yearSearch, String monthSeach,
			String yearSearchForMonth) {
		PreparedStatement preparedStatement = null;
		String strResponse = "";
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();

		Double distance1 = 0.0;
		Double distance2 = 0.0;
		Double distance3 = 0.0;
		Double distance4 = 0.0;
		Double distance5 = 0.0;
		Double distance6 = 0.0;
		Double distance7 = 0.0;
		Double distance8 = 0.0;
		Double distance9 = 0.0;
		Double distance10 = 0.0;
		Double distance11 = 0.0;
		Double distance12 = 0.0;

		Double distanceDay1 = 0.0;
		Double distanceDay2 = 0.0;
		Double distanceDay3 = 0.0;
		Double distanceDay4 = 0.0;
		Double distanceDay5 = 0.0;
		Double distanceDay6 = 0.0;
		Double distanceDay7 = 0.0;
		Double distanceDay8 = 0.0;
		Double distanceDay9 = 0.0;
		Double distanceDay10 = 0.0;
		Double distanceDay11 = 0.0;
		Double distanceDay12 = 0.0;
		Double distanceDay13 = 0.0;
		Double distanceDay14 = 0.0;
		Double distanceDay15 = 0.0;
		Double distanceDay16 = 0.0;
		Double distanceDay17 = 0.0;
		Double distanceDay18 = 0.0;
		Double distanceDay19 = 0.0;
		Double distanceDay20 = 0.0;
		Double distanceDay21 = 0.0;
		Double distanceDay22 = 0.0;
		Double distanceDay23 = 0.0;
		Double distanceDay24 = 0.0;
		Double distanceDay25 = 0.0;
		Double distanceDay26 = 0.0;
		Double distanceDay27 = 0.0;
		Double distanceDay28 = 0.0;
		Double distanceDay29 = 0.0;
		Double distanceDay30 = 0.0;
		Double distanceDay31 = 0.0;

		ArrayList<String> myLatitude1 = new ArrayList<String>();
		ArrayList<String> myLongitude1 = new ArrayList<String>();
		ArrayList<String> myDistance1 = new ArrayList<String>();

		ArrayList<String> myLatitude2 = new ArrayList<String>();
		ArrayList<String> myLongitude2 = new ArrayList<String>();
		ArrayList<String> myDistance2 = new ArrayList<String>();

		ArrayList<String> myLatitude3 = new ArrayList<String>();
		ArrayList<String> myLongitude3 = new ArrayList<String>();
		ArrayList<String> myDistance3 = new ArrayList<String>();

		ArrayList<String> myLatitude4 = new ArrayList<String>();
		ArrayList<String> myLongitude4 = new ArrayList<String>();
		ArrayList<String> myDistance4 = new ArrayList<String>();

		ArrayList<String> myLatitude5 = new ArrayList<String>();
		ArrayList<String> myLongitude5 = new ArrayList<String>();
		ArrayList<String> myDistance5 = new ArrayList<String>();

		ArrayList<String> myLatitude6 = new ArrayList<String>();
		ArrayList<String> myLongitude6 = new ArrayList<String>();
		ArrayList<String> myDistance6 = new ArrayList<String>();

		ArrayList<String> myLatitude7 = new ArrayList<String>();
		ArrayList<String> myLongitude7 = new ArrayList<String>();
		ArrayList<String> myDistance7 = new ArrayList<String>();

		ArrayList<String> myLatitude8 = new ArrayList<String>();
		ArrayList<String> myLongitude8 = new ArrayList<String>();
		ArrayList<String> myDistance8 = new ArrayList<String>();

		ArrayList<String> myLatitude9 = new ArrayList<String>();
		ArrayList<String> myLongitude9 = new ArrayList<String>();
		ArrayList<String> myDistance9 = new ArrayList<String>();

		ArrayList<String> myLatitude10 = new ArrayList<String>();
		ArrayList<String> myLongitude10 = new ArrayList<String>();
		ArrayList<String> myDistance10 = new ArrayList<String>();

		ArrayList<String> myLatitude11 = new ArrayList<String>();
		ArrayList<String> myLongitude11 = new ArrayList<String>();
		ArrayList<String> myDistance11 = new ArrayList<String>();

		ArrayList<String> myLatitude12 = new ArrayList<String>();
		ArrayList<String> myLongitude12 = new ArrayList<String>();
		ArrayList<String> myDistance12 = new ArrayList<String>();
		/////////////////////////////////////////////////////////////////////

		ArrayList<String> myLatitudeDay1 = new ArrayList<String>();
		ArrayList<String> myLongitudeDay1 = new ArrayList<String>();
		ArrayList<String> myDistanceDay1 = new ArrayList<String>();

		ArrayList<String> myLatitudeDay2 = new ArrayList<String>();
		ArrayList<String> myLongitudeDay2 = new ArrayList<String>();
		ArrayList<String> myDistanceDay2 = new ArrayList<String>();

		ArrayList<String> myLatitudeDay3 = new ArrayList<String>();
		ArrayList<String> myLongitudeDay3 = new ArrayList<String>();
		ArrayList<String> myDistanceDay3 = new ArrayList<String>();

		ArrayList<String> myLatitudeDay4 = new ArrayList<String>();
		ArrayList<String> myLongitudeDay4 = new ArrayList<String>();
		ArrayList<String> myDistanceDay4 = new ArrayList<String>();

		ArrayList<String> myLatitudeDay5 = new ArrayList<String>();
		ArrayList<String> myLongitudeDay5 = new ArrayList<String>();
		ArrayList<String> myDistanceDay5 = new ArrayList<String>();

		ArrayList<String> myLatitudeDay6 = new ArrayList<String>();
		ArrayList<String> myLongitudeDay6 = new ArrayList<String>();
		ArrayList<String> myDistanceDay6 = new ArrayList<String>();

		ArrayList<String> myLatitudeDay7 = new ArrayList<String>();
		ArrayList<String> myLongitudeDay7 = new ArrayList<String>();
		ArrayList<String> myDistanceDay7 = new ArrayList<String>();

		ArrayList<String> myLatitudeDay8 = new ArrayList<String>();
		ArrayList<String> myLongitudeDay8 = new ArrayList<String>();
		ArrayList<String> myDistanceDay8 = new ArrayList<String>();

		ArrayList<String> myLatitudeDay9 = new ArrayList<String>();
		ArrayList<String> myLongitudeDay9 = new ArrayList<String>();
		ArrayList<String> myDistanceDay9 = new ArrayList<String>();

		ArrayList<String> myLatitudeDay10 = new ArrayList<String>();
		ArrayList<String> myLongitudeDay10 = new ArrayList<String>();
		ArrayList<String> myDistanceDay10 = new ArrayList<String>();

		ArrayList<String> myLatitudeDay11 = new ArrayList<String>();
		ArrayList<String> myLongitudeDay11 = new ArrayList<String>();
		ArrayList<String> myDistanceDay11 = new ArrayList<String>();

		ArrayList<String> myLatitudeDay12 = new ArrayList<String>();
		ArrayList<String> myLongitudeDay12 = new ArrayList<String>();
		ArrayList<String> myDistanceDay12 = new ArrayList<String>();

		ArrayList<String> myLatitudeDay13 = new ArrayList<String>();
		ArrayList<String> myLongitudeDay13 = new ArrayList<String>();
		ArrayList<String> myDistanceDay13 = new ArrayList<String>();

		ArrayList<String> myLatitudeDay14 = new ArrayList<String>();
		ArrayList<String> myLongitudeDay14 = new ArrayList<String>();
		ArrayList<String> myDistanceDay14 = new ArrayList<String>();

		ArrayList<String> myLatitudeDay15 = new ArrayList<String>();
		ArrayList<String> myLongitudeDay15 = new ArrayList<String>();
		ArrayList<String> myDistanceDay15 = new ArrayList<String>();

		ArrayList<String> myLatitudeDay16 = new ArrayList<String>();
		ArrayList<String> myLongitudeDay16 = new ArrayList<String>();
		ArrayList<String> myDistanceDay16 = new ArrayList<String>();

		ArrayList<String> myLatitudeDay17 = new ArrayList<String>();
		ArrayList<String> myLongitudeDay17 = new ArrayList<String>();
		ArrayList<String> myDistanceDay17 = new ArrayList<String>();

		ArrayList<String> myLatitudeDay18 = new ArrayList<String>();
		ArrayList<String> myLongitudeDay18 = new ArrayList<String>();
		ArrayList<String> myDistanceDay18 = new ArrayList<String>();

		ArrayList<String> myLatitudeDay19 = new ArrayList<String>();
		ArrayList<String> myLongitudeDay19 = new ArrayList<String>();
		ArrayList<String> myDistanceDay19 = new ArrayList<String>();

		ArrayList<String> myLatitudeDay20 = new ArrayList<String>();
		ArrayList<String> myLongitudeDay20 = new ArrayList<String>();
		ArrayList<String> myDistanceDay20 = new ArrayList<String>();

		ArrayList<String> myLatitudeDay21 = new ArrayList<String>();
		ArrayList<String> myLongitudeDay21 = new ArrayList<String>();
		ArrayList<String> myDistanceDay21 = new ArrayList<String>();

		ArrayList<String> myLatitudeDay22 = new ArrayList<String>();
		ArrayList<String> myLongitudeDay22 = new ArrayList<String>();
		ArrayList<String> myDistanceDay22 = new ArrayList<String>();

		ArrayList<String> myLatitudeDay23 = new ArrayList<String>();
		ArrayList<String> myLongitudeDay23 = new ArrayList<String>();
		ArrayList<String> myDistanceDay23 = new ArrayList<String>();

		ArrayList<String> myLatitudeDay24 = new ArrayList<String>();
		ArrayList<String> myLongitudeDay24 = new ArrayList<String>();
		ArrayList<String> myDistanceDay24 = new ArrayList<String>();

		ArrayList<String> myLatitudeDay25 = new ArrayList<String>();
		ArrayList<String> myLongitudeDay25 = new ArrayList<String>();
		ArrayList<String> myDistanceDay25 = new ArrayList<String>();

		ArrayList<String> myLatitudeDay26 = new ArrayList<String>();
		ArrayList<String> myLongitudeDay26 = new ArrayList<String>();
		ArrayList<String> myDistanceDay26 = new ArrayList<String>();

		ArrayList<String> myLatitudeDay27 = new ArrayList<String>();
		ArrayList<String> myLongitudeDay27 = new ArrayList<String>();
		ArrayList<String> myDistanceDay27 = new ArrayList<String>();

		ArrayList<String> myLatitudeDay28 = new ArrayList<String>();
		ArrayList<String> myLongitudeDay28 = new ArrayList<String>();
		ArrayList<String> myDistanceDay28 = new ArrayList<String>();

		ArrayList<String> myLatitudeDay29 = new ArrayList<String>();
		ArrayList<String> myLongitudeDay29 = new ArrayList<String>();
		ArrayList<String> myDistanceDay29 = new ArrayList<String>();

		ArrayList<String> myLatitudeDay30 = new ArrayList<String>();
		ArrayList<String> myLongitudeDay30 = new ArrayList<String>();
		ArrayList<String> myDistanceDay30 = new ArrayList<String>();

		ArrayList<String> myLatitudeDay31 = new ArrayList<String>();
		ArrayList<String> myLongitudeDay31 = new ArrayList<String>();
		ArrayList<String> myDistanceDay31 = new ArrayList<String>();

		HashMap<String, Object> respData = new HashMap<String, Object>();
		int j = 1;
		try {
			sql.append(" SELECT LONGITUDE,LATITUDE,MONTH(CREATE_DATE) AS CAL_MONTH ,ucc.WEIGHT, ");
			sql.append(" (case when CREATE_DATE between ? and ? then DAY(CREATE_DATE) else '' end) as DAYS  ");
			sql.append(" FROM GPS g ");
			sql.append(" LEFT JOIN GPS_HISTORY gh ON gh.GPS_ID = g.GPS_ID ");
			sql.append(" LEFT JOIN user_conect_cane ucc on ucc.CANE_ID = g.CANE_ID ");
			sql.append(" WHERE gh.CREATE_DATE between ? and ? ");
			sql.append(" AND g.CANE_ID = ? ");
			preparedStatement = connection.prepareStatement(sql.toString());
			preparedStatement.setString(1, yearSearchForMonth.concat("-").concat(monthSeach).concat("-01 00:00:00"));
			preparedStatement.setString(2, yearSearchForMonth.concat("-").concat(monthSeach).concat("-31 23:59:59"));
			preparedStatement.setString(3, yearSearch.concat("-01-01 00:00:00"));
			preparedStatement.setString(4, yearSearch.concat("-12-31 23:59:59"));
			preparedStatement.setInt(5, selectCane);
			rs = preparedStatement.executeQuery();
			while ((rs != null) && (rs.next())) {

				if (rs.getInt("CAL_MONTH") == 1) {
					myLatitude1.add(rs.getString("LONGITUDE"));
					myLongitude1.add(rs.getString("LATITUDE"));
				} else if (rs.getInt("CAL_MONTH") == 2) {
					myLatitude2.add(rs.getString("LONGITUDE"));
					myLongitude2.add(rs.getString("LATITUDE"));
				} else if (rs.getInt("CAL_MONTH") == 3) {
					myLatitude3.add(rs.getString("LONGITUDE"));
					myLongitude3.add(rs.getString("LATITUDE"));
				} else if (rs.getInt("CAL_MONTH") == 4) {
					myLatitude4.add(rs.getString("LONGITUDE"));
					myLongitude4.add(rs.getString("LATITUDE"));
				} else if (rs.getInt("CAL_MONTH") == 5) {
					myLatitude5.add(rs.getString("LONGITUDE"));
					myLongitude5.add(rs.getString("LATITUDE"));
				} else if (rs.getInt("CAL_MONTH") == 6) {
					myLatitude6.add(rs.getString("LONGITUDE"));
					myLongitude6.add(rs.getString("LATITUDE"));
				} else if (rs.getInt("CAL_MONTH") == 7) {
					myLatitude7.add(rs.getString("LONGITUDE"));
					myLongitude7.add(rs.getString("LATITUDE"));
				} else if (rs.getInt("CAL_MONTH") == 8) {
					myLatitude8.add(rs.getString("LONGITUDE"));
					myLongitude8.add(rs.getString("LATITUDE"));
				} else if (rs.getInt("CAL_MONTH") == 9) {
					myLatitude9.add(rs.getString("LONGITUDE"));
					myLongitude9.add(rs.getString("LATITUDE"));
				} else if (rs.getInt("CAL_MONTH") == 10) {
					myLatitude10.add(rs.getString("LONGITUDE"));
					myLongitude10.add(rs.getString("LATITUDE"));
				} else if (rs.getInt("CAL_MONTH") == 11) {
					myLatitude11.add(rs.getString("LONGITUDE"));
					myLongitude11.add(rs.getString("LATITUDE"));
				} else if (rs.getInt("CAL_MONTH") == 12) {
					myLatitude12.add(rs.getString("LONGITUDE"));
					myLongitude12.add(rs.getString("LATITUDE"));
				}

				respData.put("WEIGHT", rs.getString("WEIGHT"));

				if (rs.getInt("DAYS") == 1) {
					myLatitudeDay1.add(rs.getString("LONGITUDE"));
					myLongitudeDay1.add(rs.getString("LATITUDE"));
				} else if (rs.getInt("DAYS") == 2) {
					myLatitudeDay2.add(rs.getString("LONGITUDE"));
					myLongitudeDay2.add(rs.getString("LATITUDE"));
				} else if (rs.getInt("DAYS") == 3) {
					myLatitudeDay3.add(rs.getString("LONGITUDE"));
					myLongitudeDay3.add(rs.getString("LATITUDE"));
				} else if (rs.getInt("DAYS") == 4) {
					myLatitudeDay4.add(rs.getString("LONGITUDE"));
					myLongitudeDay4.add(rs.getString("LATITUDE"));
				} else if (rs.getInt("DAYS") == 5) {
					myLatitudeDay5.add(rs.getString("LONGITUDE"));
					myLongitudeDay5.add(rs.getString("LATITUDE"));
				} else if (rs.getInt("DAYS") == 6) {
					myLatitudeDay6.add(rs.getString("LONGITUDE"));
					myLongitudeDay6.add(rs.getString("LATITUDE"));
				} else if (rs.getInt("DAYS") == 7) {
					myLatitudeDay7.add(rs.getString("LONGITUDE"));
					myLongitudeDay7.add(rs.getString("LATITUDE"));
				} else if (rs.getInt("DAYS") == 8) {
					myLatitudeDay8.add(rs.getString("LONGITUDE"));
					myLongitudeDay8.add(rs.getString("LATITUDE"));
				} else if (rs.getInt("DAYS") == 9) {
					myLatitudeDay9.add(rs.getString("LONGITUDE"));
					myLongitudeDay9.add(rs.getString("LATITUDE"));
				} else if (rs.getInt("DAYS") == 10) {
					myLatitudeDay10.add(rs.getString("LONGITUDE"));
					myLongitudeDay10.add(rs.getString("LATITUDE"));
				} else if (rs.getInt("DAYS") == 11) {
					myLatitudeDay11.add(rs.getString("LONGITUDE"));
					myLongitudeDay11.add(rs.getString("LATITUDE"));
				} else if (rs.getInt("DAYS") == 12) {
					myLatitudeDay12.add(rs.getString("LONGITUDE"));
					myLongitudeDay12.add(rs.getString("LATITUDE"));
				} else if (rs.getInt("DAYS") == 13) {
					myLatitudeDay13.add(rs.getString("LONGITUDE"));
					myLongitudeDay13.add(rs.getString("LATITUDE"));
				} else if (rs.getInt("DAYS") == 14) {
					myLatitudeDay14.add(rs.getString("LONGITUDE"));
					myLongitudeDay14.add(rs.getString("LATITUDE"));
				} else if (rs.getInt("DAYS") == 15) {
					myLatitudeDay15.add(rs.getString("LONGITUDE"));
					myLongitudeDay15.add(rs.getString("LATITUDE"));
				} else if (rs.getInt("DAYS") == 16) {
					myLatitudeDay16.add(rs.getString("LONGITUDE"));
					myLongitudeDay16.add(rs.getString("LATITUDE"));
				} else if (rs.getInt("DAYS") == 17) {
					myLatitudeDay17.add(rs.getString("LONGITUDE"));
					myLongitudeDay17.add(rs.getString("LATITUDE"));
				} else if (rs.getInt("DAYS") == 18) {
					myLatitudeDay18.add(rs.getString("LONGITUDE"));
					myLongitudeDay18.add(rs.getString("LATITUDE"));
				} else if (rs.getInt("DAYS") == 19) {
					myLatitudeDay19.add(rs.getString("LONGITUDE"));
					myLongitudeDay19.add(rs.getString("LATITUDE"));
				} else if (rs.getInt("DAYS") == 20) {
					myLatitudeDay20.add(rs.getString("LONGITUDE"));
					myLongitudeDay20.add(rs.getString("LATITUDE"));
				} else if (rs.getInt("DAYS") == 21) {
					myLatitudeDay21.add(rs.getString("LONGITUDE"));
					myLongitudeDay21.add(rs.getString("LATITUDE"));
				} else if (rs.getInt("DAYS") == 22) {
					myLatitudeDay22.add(rs.getString("LONGITUDE"));
					myLongitudeDay22.add(rs.getString("LATITUDE"));
				} else if (rs.getInt("DAYS") == 23) {
					myLatitudeDay23.add(rs.getString("LONGITUDE"));
					myLongitudeDay23.add(rs.getString("LATITUDE"));
				} else if (rs.getInt("DAYS") == 24) {
					myLatitudeDay24.add(rs.getString("LONGITUDE"));
					myLongitudeDay24.add(rs.getString("LATITUDE"));
				} else if (rs.getInt("DAYS") == 25) {
					myLatitudeDay25.add(rs.getString("LONGITUDE"));
					myLongitudeDay25.add(rs.getString("LATITUDE"));
				} else if (rs.getInt("DAYS") == 26) {
					myLatitudeDay26.add(rs.getString("LONGITUDE"));
					myLongitudeDay26.add(rs.getString("LATITUDE"));
				} else if (rs.getInt("DAYS") == 27) {
					myLatitudeDay27.add(rs.getString("LONGITUDE"));
					myLongitudeDay27.add(rs.getString("LATITUDE"));
				} else if (rs.getInt("DAYS") == 28) {
					myLatitudeDay28.add(rs.getString("LONGITUDE"));
					myLongitudeDay28.add(rs.getString("LATITUDE"));
				} else if (rs.getInt("DAYS") == 29) {
					myLatitudeDay29.add(rs.getString("LONGITUDE"));
					myLongitudeDay29.add(rs.getString("LATITUDE"));
				} else if (rs.getInt("DAYS") == 30) {
					myLatitudeDay30.add(rs.getString("LONGITUDE"));
					myLongitudeDay30.add(rs.getString("LATITUDE"));
				} else if (rs.getInt("DAYS") == 31) {
					myLatitudeDay31.add(rs.getString("LONGITUDE"));
					myLongitudeDay31.add(rs.getString("LATITUDE"));
				}
			}
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitude1.size(); i++) {
				if (i < myLatitude1.size() - 1 && myLatitude1.size() % 2 == 1) {
					myDistance1.add(Double.toString(distance(Double.parseDouble(myLatitude1.get(i)),
							Double.parseDouble(myLatitude1.get(j)), Double.parseDouble(myLongitude1.get(i)),
							Double.parseDouble(myLongitude1.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitude1.size() - 1 && myLatitude1.size() % 2 == 0) {
					myDistance1.add(Double.toString(distance(Double.parseDouble(myLatitude1.get(i)),
							Double.parseDouble(myLatitude1.get(j)), Double.parseDouble(myLongitude1.get(i)),
							Double.parseDouble(myLongitude1.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitude2.size(); i++) {
				if (i < myLatitude2.size() - 1 && myLatitude2.size() % 2 == 1) {
					myDistance2.add(Double.toString(distance(Double.parseDouble(myLatitude2.get(i)),
							Double.parseDouble(myLatitude2.get(j)), Double.parseDouble(myLongitude2.get(i)),
							Double.parseDouble(myLongitude2.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitude2.size() - 1 && myLatitude2.size() % 2 == 0) {
					myDistance2.add(Double.toString(distance(Double.parseDouble(myLatitude2.get(i)),
							Double.parseDouble(myLatitude2.get(j)), Double.parseDouble(myLongitude2.get(i)),
							Double.parseDouble(myLongitude2.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitude3.size(); i++) {
				if (i < myLatitude3.size() - 1 && myLatitude3.size() % 2 == 1) {
					myDistance3.add(Double.toString(distance(Double.parseDouble(myLatitude3.get(i)),
							Double.parseDouble(myLatitude3.get(j)), Double.parseDouble(myLongitude3.get(i)),
							Double.parseDouble(myLongitude3.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitude3.size() - 1 && myLatitude3.size() % 2 == 0) {
					myDistance3.add(Double.toString(distance(Double.parseDouble(myLatitude3.get(i)),
							Double.parseDouble(myLatitude3.get(j)), Double.parseDouble(myLongitude3.get(i)),
							Double.parseDouble(myLongitude3.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitude4.size(); i++) {
				if (i < myLatitude4.size() - 1 && myLatitude4.size() % 2 == 1) {
					myDistance4.add(Double.toString(distance(Double.parseDouble(myLatitude4.get(i)),
							Double.parseDouble(myLatitude4.get(j)), Double.parseDouble(myLongitude4.get(i)),
							Double.parseDouble(myLongitude4.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitude4.size() - 1 && myLatitude4.size() % 2 == 0) {
					myDistance4.add(Double.toString(distance(Double.parseDouble(myLatitude4.get(i)),
							Double.parseDouble(myLatitude4.get(j)), Double.parseDouble(myLongitude4.get(i)),
							Double.parseDouble(myLongitude4.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitude5.size(); i++) {
				if (i < myLatitude5.size() - 1 && myLatitude5.size() % 2 == 1) {
					myDistance5.add(Double.toString(distance(Double.parseDouble(myLatitude5.get(i)),
							Double.parseDouble(myLatitude5.get(j)), Double.parseDouble(myLongitude5.get(i)),
							Double.parseDouble(myLongitude5.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitude5.size() - 1 && myLatitude5.size() % 2 == 0) {
					myDistance5.add(Double.toString(distance(Double.parseDouble(myLatitude5.get(i)),
							Double.parseDouble(myLatitude5.get(j)), Double.parseDouble(myLongitude5.get(i)),
							Double.parseDouble(myLongitude5.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitude6.size(); i++) {
				if (i < myLatitude6.size() - 1 && myLatitude6.size() % 2 == 1) {
					myDistance6.add(Double.toString(distance(Double.parseDouble(myLatitude6.get(i)),
							Double.parseDouble(myLatitude6.get(j)), Double.parseDouble(myLongitude6.get(i)),
							Double.parseDouble(myLongitude6.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitude6.size() - 1 && myLatitude6.size() % 2 == 0) {
					myDistance6.add(Double.toString(distance(Double.parseDouble(myLatitude6.get(i)),
							Double.parseDouble(myLatitude6.get(j)), Double.parseDouble(myLongitude6.get(i)),
							Double.parseDouble(myLongitude6.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitude7.size(); i++) {
				if (i < myLatitude7.size() - 1 && myLatitude7.size() % 2 == 1) {
					myDistance7.add(Double.toString(distance(Double.parseDouble(myLatitude7.get(i)),
							Double.parseDouble(myLatitude7.get(j)), Double.parseDouble(myLongitude7.get(i)),
							Double.parseDouble(myLongitude7.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitude7.size() - 1 && myLatitude7.size() % 2 == 0) {
					myDistance7.add(Double.toString(distance(Double.parseDouble(myLatitude7.get(i)),
							Double.parseDouble(myLatitude7.get(j)), Double.parseDouble(myLongitude7.get(i)),
							Double.parseDouble(myLongitude7.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitude8.size(); i++) {
				if (i < myLatitude8.size() - 1 && myLatitude8.size() % 2 == 1) {
					myDistance8.add(Double.toString(distance(Double.parseDouble(myLatitude8.get(i)),
							Double.parseDouble(myLatitude8.get(j)), Double.parseDouble(myLongitude8.get(i)),
							Double.parseDouble(myLongitude8.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitude8.size() - 1 && myLatitude8.size() % 2 == 0) {
					myDistance8.add(Double.toString(distance(Double.parseDouble(myLatitude8.get(i)),
							Double.parseDouble(myLatitude8.get(j)), Double.parseDouble(myLongitude8.get(i)),
							Double.parseDouble(myLongitude8.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitude9.size(); i++) {
				if (i < myLatitude9.size() - 1 && myLatitude9.size() % 2 == 1) {
					myDistance9.add(Double.toString(distance(Double.parseDouble(myLatitude9.get(i)),
							Double.parseDouble(myLatitude9.get(j)), Double.parseDouble(myLongitude9.get(i)),
							Double.parseDouble(myLongitude9.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitude9.size() - 1 && myLatitude9.size() % 2 == 0) {
					myDistance9.add(Double.toString(distance(Double.parseDouble(myLatitude9.get(i)),
							Double.parseDouble(myLatitude9.get(j)), Double.parseDouble(myLongitude9.get(i)),
							Double.parseDouble(myLongitude9.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitude10.size(); i++) {
				if (i < myLatitude10.size() - 1 && myLatitude10.size() % 2 == 1) {
					myDistance10.add(Double.toString(distance(Double.parseDouble(myLatitude10.get(i)),
							Double.parseDouble(myLatitude10.get(j)), Double.parseDouble(myLongitude10.get(i)),
							Double.parseDouble(myLongitude10.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitude10.size() - 1 && myLatitude10.size() % 2 == 0) {
					myDistance10.add(Double.toString(distance(Double.parseDouble(myLatitude10.get(i)),
							Double.parseDouble(myLatitude10.get(j)), Double.parseDouble(myLongitude10.get(i)),
							Double.parseDouble(myLongitude10.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitude11.size(); i++) {
				if (i < myLatitude11.size() - 1 && myLatitude11.size() % 2 == 1) {
					myDistance11.add(Double.toString(distance(Double.parseDouble(myLatitude11.get(i)),
							Double.parseDouble(myLatitude11.get(j)), Double.parseDouble(myLongitude11.get(i)),
							Double.parseDouble(myLongitude11.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitude11.size() - 1 && myLatitude11.size() % 2 == 0) {
					myDistance11.add(Double.toString(distance(Double.parseDouble(myLatitude11.get(i)),
							Double.parseDouble(myLatitude11.get(j)), Double.parseDouble(myLongitude11.get(i)),
							Double.parseDouble(myLongitude11.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitude12.size(); i++) {
				if (i < myLatitude12.size() - 1 && myLatitude12.size() % 2 == 1) {
					myDistance12.add(Double.toString(distance(Double.parseDouble(myLatitude12.get(i)),
							Double.parseDouble(myLatitude12.get(j)), Double.parseDouble(myLongitude12.get(i)),
							Double.parseDouble(myLongitude12.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitude12.size() - 1 && myLatitude12.size() % 2 == 0) {
					myDistance12.add(Double.toString(distance(Double.parseDouble(myLatitude12.get(i)),
							Double.parseDouble(myLatitude12.get(j)), Double.parseDouble(myLongitude12.get(i)),
							Double.parseDouble(myLongitude12.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;

			///////////////// DAY //////////////////////////
			///////////////// DAY //////////////////////////

			for (int i = 0; i < myLatitudeDay1.size(); i++) {
				if (i < myLatitudeDay1.size() - 1 && myLatitudeDay1.size() % 2 == 1) {
					myDistanceDay1.add(Double.toString(distance(Double.parseDouble(myLatitudeDay1.get(i)),
							Double.parseDouble(myLatitudeDay1.get(j)), Double.parseDouble(myLongitudeDay1.get(i)),
							Double.parseDouble(myLongitudeDay1.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitudeDay1.size() - 1 && myLatitudeDay1.size() % 2 == 0) {
					myDistanceDay1.add(Double.toString(distance(Double.parseDouble(myLatitudeDay1.get(i)),
							Double.parseDouble(myLatitudeDay1.get(j)), Double.parseDouble(myLongitudeDay1.get(i)),
							Double.parseDouble(myLongitudeDay1.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitudeDay2.size(); i++) {
				if (i < myLatitudeDay2.size() - 1 && myLatitudeDay2.size() % 2 == 1) {
					myDistanceDay2.add(Double.toString(distance(Double.parseDouble(myLatitudeDay2.get(i)),
							Double.parseDouble(myLatitudeDay2.get(j)), Double.parseDouble(myLongitudeDay2.get(i)),
							Double.parseDouble(myLongitudeDay2.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitudeDay2.size() - 1 && myLatitudeDay2.size() % 2 == 0) {
					myDistanceDay2.add(Double.toString(distance(Double.parseDouble(myLatitudeDay2.get(i)),
							Double.parseDouble(myLatitudeDay2.get(j)), Double.parseDouble(myLongitudeDay2.get(i)),
							Double.parseDouble(myLongitudeDay2.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitudeDay3.size(); i++) {
				if (i < myLatitudeDay3.size() - 1 && myLatitudeDay3.size() % 2 == 1) {
					myDistanceDay3.add(Double.toString(distance(Double.parseDouble(myLatitudeDay3.get(i)),
							Double.parseDouble(myLatitudeDay3.get(j)), Double.parseDouble(myLongitudeDay3.get(i)),
							Double.parseDouble(myLongitudeDay3.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitudeDay3.size() - 1 && myLatitudeDay3.size() % 2 == 0) {
					myDistanceDay3.add(Double.toString(distance(Double.parseDouble(myLatitudeDay3.get(i)),
							Double.parseDouble(myLatitudeDay3.get(j)), Double.parseDouble(myLongitudeDay3.get(i)),
							Double.parseDouble(myLongitudeDay3.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitudeDay4.size(); i++) {
				if (i < myLatitudeDay4.size() - 1 && myLatitudeDay4.size() % 2 == 1) {
					myDistanceDay4.add(Double.toString(distance(Double.parseDouble(myLatitudeDay4.get(i)),
							Double.parseDouble(myLatitudeDay4.get(j)), Double.parseDouble(myLongitudeDay4.get(i)),
							Double.parseDouble(myLongitudeDay4.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitudeDay4.size() - 1 && myLatitudeDay4.size() % 2 == 0) {
					myDistanceDay4.add(Double.toString(distance(Double.parseDouble(myLatitudeDay4.get(i)),
							Double.parseDouble(myLatitudeDay4.get(j)), Double.parseDouble(myLongitudeDay4.get(i)),
							Double.parseDouble(myLongitudeDay4.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitudeDay5.size(); i++) {
				if (i < myLatitudeDay5.size() - 1 && myLatitudeDay5.size() % 2 == 1) {
					myDistanceDay5.add(Double.toString(distance(Double.parseDouble(myLatitudeDay5.get(i)),
							Double.parseDouble(myLatitudeDay5.get(j)), Double.parseDouble(myLongitudeDay5.get(i)),
							Double.parseDouble(myLongitudeDay5.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitudeDay5.size() - 1 && myLatitudeDay5.size() % 2 == 0) {
					myDistanceDay5.add(Double.toString(distance(Double.parseDouble(myLatitudeDay5.get(i)),
							Double.parseDouble(myLatitudeDay5.get(j)), Double.parseDouble(myLongitudeDay5.get(i)),
							Double.parseDouble(myLongitudeDay5.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitudeDay6.size(); i++) {
				if (i < myLatitudeDay6.size() - 1 && myLatitudeDay6.size() % 2 == 1) {
					myDistanceDay6.add(Double.toString(distance(Double.parseDouble(myLatitudeDay6.get(i)),
							Double.parseDouble(myLatitudeDay6.get(j)), Double.parseDouble(myLongitudeDay6.get(i)),
							Double.parseDouble(myLongitudeDay6.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitudeDay6.size() - 1 && myLatitudeDay6.size() % 2 == 0) {
					myDistanceDay6.add(Double.toString(distance(Double.parseDouble(myLatitudeDay6.get(i)),
							Double.parseDouble(myLatitudeDay6.get(j)), Double.parseDouble(myLongitudeDay6.get(i)),
							Double.parseDouble(myLongitudeDay6.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitudeDay7.size(); i++) {
				if (i < myLatitudeDay7.size() - 1 && myLatitudeDay7.size() % 2 == 1) {
					myDistanceDay7.add(Double.toString(distance(Double.parseDouble(myLatitudeDay7.get(i)),
							Double.parseDouble(myLatitudeDay7.get(j)), Double.parseDouble(myLongitudeDay7.get(i)),
							Double.parseDouble(myLongitudeDay7.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitudeDay7.size() - 1 && myLatitudeDay7.size() % 2 == 0) {
					myDistanceDay7.add(Double.toString(distance(Double.parseDouble(myLatitudeDay7.get(i)),
							Double.parseDouble(myLatitudeDay7.get(j)), Double.parseDouble(myLongitudeDay7.get(i)),
							Double.parseDouble(myLongitudeDay7.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitudeDay8.size(); i++) {
				if (i < myLatitudeDay8.size() - 1 && myLatitudeDay8.size() % 2 == 1) {
					myDistanceDay8.add(Double.toString(distance(Double.parseDouble(myLatitudeDay8.get(i)),
							Double.parseDouble(myLatitudeDay8.get(j)), Double.parseDouble(myLongitudeDay8.get(i)),
							Double.parseDouble(myLongitudeDay8.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitudeDay8.size() - 1 && myLatitudeDay8.size() % 2 == 0) {
					myDistanceDay8.add(Double.toString(distance(Double.parseDouble(myLatitudeDay8.get(i)),
							Double.parseDouble(myLatitudeDay8.get(j)), Double.parseDouble(myLongitudeDay8.get(i)),
							Double.parseDouble(myLongitudeDay8.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitudeDay9.size(); i++) {
				if (i < myLatitudeDay9.size() - 1 && myLatitudeDay9.size() % 2 == 1) {
					myDistanceDay9.add(Double.toString(distance(Double.parseDouble(myLatitudeDay9.get(i)),
							Double.parseDouble(myLatitudeDay9.get(j)), Double.parseDouble(myLongitudeDay9.get(i)),
							Double.parseDouble(myLongitudeDay9.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitudeDay9.size() - 1 && myLatitudeDay9.size() % 2 == 0) {
					myDistanceDay9.add(Double.toString(distance(Double.parseDouble(myLatitudeDay9.get(i)),
							Double.parseDouble(myLatitudeDay9.get(j)), Double.parseDouble(myLongitudeDay9.get(i)),
							Double.parseDouble(myLongitudeDay9.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitudeDay10.size(); i++) {
				if (i < myLatitudeDay10.size() - 1 && myLatitudeDay10.size() % 2 == 1) {
					myDistanceDay10.add(Double.toString(distance(Double.parseDouble(myLatitudeDay10.get(i)),
							Double.parseDouble(myLatitudeDay10.get(j)), Double.parseDouble(myLongitudeDay10.get(i)),
							Double.parseDouble(myLongitudeDay10.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitudeDay10.size() - 1 && myLatitudeDay10.size() % 2 == 0) {
					myDistanceDay10.add(Double.toString(distance(Double.parseDouble(myLatitudeDay10.get(i)),
							Double.parseDouble(myLatitudeDay10.get(j)), Double.parseDouble(myLongitudeDay10.get(i)),
							Double.parseDouble(myLongitudeDay10.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitudeDay11.size(); i++) {
				if (i < myLatitudeDay11.size() - 1 && myLatitudeDay11.size() % 2 == 1) {
					myDistanceDay11.add(Double.toString(distance(Double.parseDouble(myLatitudeDay11.get(i)),
							Double.parseDouble(myLatitudeDay11.get(j)), Double.parseDouble(myLongitudeDay11.get(i)),
							Double.parseDouble(myLongitudeDay11.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitudeDay11.size() - 1 && myLatitudeDay11.size() % 2 == 0) {
					myDistanceDay11.add(Double.toString(distance(Double.parseDouble(myLatitudeDay11.get(i)),
							Double.parseDouble(myLatitudeDay11.get(j)), Double.parseDouble(myLongitudeDay11.get(i)),
							Double.parseDouble(myLongitudeDay11.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitudeDay12.size(); i++) {
				if (i < myLatitudeDay12.size() - 1 && myLatitudeDay12.size() % 2 == 1) {
					myDistanceDay12.add(Double.toString(distance(Double.parseDouble(myLatitudeDay12.get(i)),
							Double.parseDouble(myLatitudeDay12.get(j)), Double.parseDouble(myLongitudeDay12.get(i)),
							Double.parseDouble(myLongitudeDay12.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitudeDay12.size() - 1 && myLatitudeDay12.size() % 2 == 0) {
					myDistanceDay12.add(Double.toString(distance(Double.parseDouble(myLatitudeDay12.get(i)),
							Double.parseDouble(myLatitudeDay12.get(j)), Double.parseDouble(myLongitudeDay12.get(i)),
							Double.parseDouble(myLongitudeDay12.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitudeDay13.size(); i++) {
				if (i < myLatitudeDay13.size() - 1 && myLatitudeDay13.size() % 2 == 1) {
					myDistanceDay13.add(Double.toString(distance(Double.parseDouble(myLatitudeDay13.get(i)),
							Double.parseDouble(myLatitudeDay13.get(j)), Double.parseDouble(myLongitudeDay13.get(i)),
							Double.parseDouble(myLongitudeDay13.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitudeDay13.size() - 1 && myLatitudeDay13.size() % 2 == 0) {
					myDistanceDay13.add(Double.toString(distance(Double.parseDouble(myLatitudeDay13.get(i)),
							Double.parseDouble(myLatitudeDay13.get(j)), Double.parseDouble(myLongitudeDay13.get(i)),
							Double.parseDouble(myLongitudeDay13.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitudeDay14.size(); i++) {
				if (i < myLatitudeDay14.size() - 1 && myLatitudeDay14.size() % 2 == 1) {
					myDistanceDay14.add(Double.toString(distance(Double.parseDouble(myLatitudeDay14.get(i)),
							Double.parseDouble(myLatitudeDay14.get(j)), Double.parseDouble(myLongitudeDay14.get(i)),
							Double.parseDouble(myLongitudeDay14.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitudeDay14.size() - 1 && myLatitudeDay14.size() % 2 == 0) {
					myDistanceDay14.add(Double.toString(distance(Double.parseDouble(myLatitudeDay14.get(i)),
							Double.parseDouble(myLatitudeDay14.get(j)), Double.parseDouble(myLongitudeDay14.get(i)),
							Double.parseDouble(myLongitudeDay14.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitudeDay15.size(); i++) {
				if (i < myLatitudeDay15.size() - 1 && myLatitudeDay15.size() % 2 == 1) {
					myDistanceDay15.add(Double.toString(distance(Double.parseDouble(myLatitudeDay15.get(i)),
							Double.parseDouble(myLatitudeDay15.get(j)), Double.parseDouble(myLongitudeDay15.get(i)),
							Double.parseDouble(myLongitudeDay15.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitudeDay15.size() - 1 && myLatitudeDay15.size() % 2 == 0) {
					myDistanceDay15.add(Double.toString(distance(Double.parseDouble(myLatitudeDay15.get(i)),
							Double.parseDouble(myLatitudeDay15.get(j)), Double.parseDouble(myLongitudeDay15.get(i)),
							Double.parseDouble(myLongitudeDay15.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitudeDay16.size(); i++) {
				if (i < myLatitudeDay16.size() - 1 && myLatitudeDay16.size() % 2 == 1) {
					myDistanceDay16.add(Double.toString(distance(Double.parseDouble(myLatitudeDay16.get(i)),
							Double.parseDouble(myLatitudeDay16.get(j)), Double.parseDouble(myLongitudeDay16.get(i)),
							Double.parseDouble(myLongitudeDay16.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitudeDay16.size() - 1 && myLatitudeDay16.size() % 2 == 0) {
					myDistanceDay16.add(Double.toString(distance(Double.parseDouble(myLatitudeDay16.get(i)),
							Double.parseDouble(myLatitudeDay16.get(j)), Double.parseDouble(myLongitudeDay16.get(i)),
							Double.parseDouble(myLongitudeDay16.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitudeDay17.size(); i++) {
				if (i < myLatitudeDay17.size() - 1 && myLatitudeDay17.size() % 2 == 1) {
					myDistanceDay17.add(Double.toString(distance(Double.parseDouble(myLatitudeDay17.get(i)),
							Double.parseDouble(myLatitudeDay17.get(j)), Double.parseDouble(myLongitudeDay17.get(i)),
							Double.parseDouble(myLongitudeDay17.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitudeDay17.size() - 1 && myLatitudeDay17.size() % 2 == 0) {
					myDistanceDay17.add(Double.toString(distance(Double.parseDouble(myLatitudeDay17.get(i)),
							Double.parseDouble(myLatitudeDay17.get(j)), Double.parseDouble(myLongitudeDay17.get(i)),
							Double.parseDouble(myLongitudeDay17.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitudeDay18.size(); i++) {
				if (i < myLatitudeDay18.size() - 1 && myLatitudeDay18.size() % 2 == 1) {
					myDistanceDay12.add(Double.toString(distance(Double.parseDouble(myLatitudeDay18.get(i)),
							Double.parseDouble(myLatitudeDay18.get(j)), Double.parseDouble(myLongitudeDay18.get(i)),
							Double.parseDouble(myLongitudeDay18.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitudeDay18.size() - 1 && myLatitudeDay18.size() % 2 == 0) {
					myDistanceDay18.add(Double.toString(distance(Double.parseDouble(myLatitudeDay18.get(i)),
							Double.parseDouble(myLatitudeDay18.get(j)), Double.parseDouble(myLongitudeDay18.get(i)),
							Double.parseDouble(myLongitudeDay18.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitudeDay19.size(); i++) {
				if (i < myLatitudeDay19.size() - 1 && myLatitudeDay19.size() % 2 == 1) {
					myDistanceDay19.add(Double.toString(distance(Double.parseDouble(myLatitudeDay19.get(i)),
							Double.parseDouble(myLatitudeDay19.get(j)), Double.parseDouble(myLongitudeDay19.get(i)),
							Double.parseDouble(myLongitudeDay19.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitudeDay19.size() - 1 && myLatitudeDay19.size() % 2 == 0) {
					myDistanceDay19.add(Double.toString(distance(Double.parseDouble(myLatitudeDay19.get(i)),
							Double.parseDouble(myLatitudeDay19.get(j)), Double.parseDouble(myLongitudeDay19.get(i)),
							Double.parseDouble(myLongitudeDay19.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitudeDay20.size(); i++) {
				if (i < myLatitudeDay20.size() - 1 && myLatitudeDay20.size() % 2 == 1) {
					myDistanceDay20.add(Double.toString(distance(Double.parseDouble(myLatitudeDay20.get(i)),
							Double.parseDouble(myLatitudeDay20.get(j)), Double.parseDouble(myLongitudeDay20.get(i)),
							Double.parseDouble(myLongitudeDay20.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitudeDay20.size() - 1 && myLatitudeDay20.size() % 2 == 0) {
					myDistanceDay20.add(Double.toString(distance(Double.parseDouble(myLatitudeDay20.get(i)),
							Double.parseDouble(myLatitudeDay20.get(j)), Double.parseDouble(myLongitudeDay20.get(i)),
							Double.parseDouble(myLongitudeDay20.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitudeDay21.size(); i++) {
				if (i < myLatitudeDay21.size() - 1 && myLatitudeDay21.size() % 2 == 1) {
					myDistanceDay21.add(Double.toString(distance(Double.parseDouble(myLatitudeDay21.get(i)),
							Double.parseDouble(myLatitudeDay21.get(j)), Double.parseDouble(myLongitudeDay21.get(i)),
							Double.parseDouble(myLongitudeDay21.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitudeDay21.size() - 1 && myLatitudeDay21.size() % 2 == 0) {
					myDistanceDay21.add(Double.toString(distance(Double.parseDouble(myLatitudeDay21.get(i)),
							Double.parseDouble(myLatitudeDay21.get(j)), Double.parseDouble(myLongitudeDay21.get(i)),
							Double.parseDouble(myLongitudeDay21.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitudeDay22.size(); i++) {
				if (i < myLatitudeDay22.size() - 1 && myLatitudeDay22.size() % 2 == 1) {
					myDistanceDay22.add(Double.toString(distance(Double.parseDouble(myLatitudeDay22.get(i)),
							Double.parseDouble(myLatitudeDay22.get(j)), Double.parseDouble(myLongitudeDay22.get(i)),
							Double.parseDouble(myLongitudeDay22.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitudeDay22.size() - 1 && myLatitudeDay22.size() % 2 == 0) {
					myDistanceDay22.add(Double.toString(distance(Double.parseDouble(myLatitudeDay22.get(i)),
							Double.parseDouble(myLatitudeDay22.get(j)), Double.parseDouble(myLongitudeDay22.get(i)),
							Double.parseDouble(myLongitudeDay22.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitudeDay23.size(); i++) {
				if (i < myLatitudeDay23.size() - 1 && myLatitudeDay23.size() % 2 == 1) {
					myDistanceDay21.add(Double.toString(distance(Double.parseDouble(myLatitudeDay23.get(i)),
							Double.parseDouble(myLatitudeDay23.get(j)), Double.parseDouble(myLongitudeDay23.get(i)),
							Double.parseDouble(myLongitudeDay23.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitudeDay23.size() - 1 && myLatitudeDay23.size() % 2 == 0) {
					myDistanceDay21.add(Double.toString(distance(Double.parseDouble(myLatitudeDay23.get(i)),
							Double.parseDouble(myLatitudeDay23.get(j)), Double.parseDouble(myLongitudeDay23.get(i)),
							Double.parseDouble(myLongitudeDay23.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitudeDay24.size(); i++) {
				if (i < myLatitudeDay24.size() - 1 && myLatitudeDay24.size() % 2 == 1) {
					myDistanceDay24.add(Double.toString(distance(Double.parseDouble(myLatitudeDay24.get(i)),
							Double.parseDouble(myLatitudeDay24.get(j)), Double.parseDouble(myLongitudeDay24.get(i)),
							Double.parseDouble(myLongitudeDay24.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitudeDay24.size() - 1 && myLatitudeDay20.size() % 2 == 0) {
					myDistanceDay24.add(Double.toString(distance(Double.parseDouble(myLatitudeDay24.get(i)),
							Double.parseDouble(myLatitudeDay24.get(j)), Double.parseDouble(myLongitudeDay24.get(i)),
							Double.parseDouble(myLongitudeDay24.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitudeDay25.size(); i++) {
				if (i < myLatitudeDay25.size() - 1 && myLatitudeDay25.size() % 2 == 1) {
					myDistanceDay25.add(Double.toString(distance(Double.parseDouble(myLatitudeDay25.get(i)),
							Double.parseDouble(myLatitudeDay25.get(j)), Double.parseDouble(myLongitudeDay25.get(i)),
							Double.parseDouble(myLongitudeDay25.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitudeDay25.size() - 1 && myLatitudeDay25.size() % 2 == 0) {
					myDistanceDay25.add(Double.toString(distance(Double.parseDouble(myLatitudeDay25.get(i)),
							Double.parseDouble(myLatitudeDay25.get(j)), Double.parseDouble(myLongitudeDay25.get(i)),
							Double.parseDouble(myLongitudeDay25.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitudeDay26.size(); i++) {
				if (i < myLatitudeDay26.size() - 1 && myLatitudeDay26.size() % 2 == 1) {
					myDistanceDay26.add(Double.toString(distance(Double.parseDouble(myLatitudeDay26.get(i)),
							Double.parseDouble(myLatitudeDay26.get(j)), Double.parseDouble(myLongitudeDay26.get(i)),
							Double.parseDouble(myLongitudeDay26.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitudeDay26.size() - 1 && myLatitudeDay26.size() % 2 == 0) {
					myDistanceDay26.add(Double.toString(distance(Double.parseDouble(myLatitudeDay26.get(i)),
							Double.parseDouble(myLatitudeDay26.get(j)), Double.parseDouble(myLongitudeDay26.get(i)),
							Double.parseDouble(myLongitudeDay26.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitudeDay27.size(); i++) {
				if (i < myLatitudeDay27.size() - 1 && myLatitudeDay27.size() % 2 == 1) {
					myDistanceDay27.add(Double.toString(distance(Double.parseDouble(myLatitudeDay27.get(i)),
							Double.parseDouble(myLatitudeDay27.get(j)), Double.parseDouble(myLongitudeDay27.get(i)),
							Double.parseDouble(myLongitudeDay27.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitudeDay27.size() - 1 && myLatitudeDay27.size() % 2 == 0) {
					myDistanceDay27.add(Double.toString(distance(Double.parseDouble(myLatitudeDay27.get(i)),
							Double.parseDouble(myLatitudeDay27.get(j)), Double.parseDouble(myLongitudeDay27.get(i)),
							Double.parseDouble(myLongitudeDay27.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitudeDay28.size(); i++) {
				if (i < myLatitudeDay28.size() - 1 && myLatitudeDay28.size() % 2 == 1) {
					myDistanceDay28.add(Double.toString(distance(Double.parseDouble(myLatitudeDay28.get(i)),
							Double.parseDouble(myLatitudeDay28.get(j)), Double.parseDouble(myLongitudeDay28.get(i)),
							Double.parseDouble(myLongitudeDay28.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitudeDay28.size() - 1 && myLatitudeDay28.size() % 2 == 0) {
					myDistanceDay28.add(Double.toString(distance(Double.parseDouble(myLatitudeDay28.get(i)),
							Double.parseDouble(myLatitudeDay28.get(j)), Double.parseDouble(myLongitudeDay28.get(i)),
							Double.parseDouble(myLongitudeDay28.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitudeDay29.size(); i++) {
				if (i < myLatitudeDay29.size() - 1 && myLatitudeDay29.size() % 2 == 1) {
					myDistanceDay29.add(Double.toString(distance(Double.parseDouble(myLatitudeDay29.get(i)),
							Double.parseDouble(myLatitudeDay29.get(j)), Double.parseDouble(myLongitudeDay29.get(i)),
							Double.parseDouble(myLongitudeDay29.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitudeDay29.size() - 1 && myLatitudeDay29.size() % 2 == 0) {
					myDistanceDay29.add(Double.toString(distance(Double.parseDouble(myLatitudeDay29.get(i)),
							Double.parseDouble(myLatitudeDay29.get(j)), Double.parseDouble(myLongitudeDay29.get(i)),
							Double.parseDouble(myLongitudeDay29.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitudeDay30.size(); i++) {
				if (i < myLatitudeDay30.size() - 1 && myLatitudeDay30.size() % 2 == 1) {
					myDistanceDay30.add(Double.toString(distance(Double.parseDouble(myLatitudeDay30.get(i)),
							Double.parseDouble(myLatitudeDay30.get(j)), Double.parseDouble(myLongitudeDay30.get(i)),
							Double.parseDouble(myLongitudeDay30.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitudeDay30.size() - 1 && myLatitudeDay30.size() % 2 == 0) {
					myDistanceDay30.add(Double.toString(distance(Double.parseDouble(myLatitudeDay30.get(i)),
							Double.parseDouble(myLatitudeDay30.get(j)), Double.parseDouble(myLongitudeDay30.get(i)),
							Double.parseDouble(myLongitudeDay30.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			///////////////////////////////////////////////////////////////////
			for (int i = 0; i < myLatitudeDay31.size(); i++) {
				if (i < myLatitudeDay31.size() - 1 && myLatitudeDay31.size() % 2 == 1) {
					myDistanceDay31.add(Double.toString(distance(Double.parseDouble(myLatitudeDay31.get(i)),
							Double.parseDouble(myLatitudeDay31.get(j)), Double.parseDouble(myLongitudeDay31.get(i)),
							Double.parseDouble(myLongitudeDay31.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitudeDay31.size() - 1 && myLatitudeDay31.size() % 2 == 0) {
					myDistanceDay31.add(Double.toString(distance(Double.parseDouble(myLatitudeDay31.get(i)),
							Double.parseDouble(myLatitudeDay31.get(j)), Double.parseDouble(myLongitudeDay31.get(i)),
							Double.parseDouble(myLongitudeDay31.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;
			
			for (int i = 0; i < myDistance1.size(); i++) {
				distance1 += Double.parseDouble(myDistance1.get(i));
			}
			for (int i = 0; i < myDistance2.size(); i++) {
				distance2 += Double.parseDouble(myDistance2.get(i));
			}
			for (int i = 0; i < myDistance3.size(); i++) {
				distance3 += Double.parseDouble(myDistance3.get(i));
			}
			for (int i = 0; i < myDistance4.size(); i++) {
				distance4 += Double.parseDouble(myDistance4.get(i));
			}
			for (int i = 0; i < myDistance5.size(); i++) {
				distance5 += Double.parseDouble(myDistance5.get(i));
			}
			for (int i = 0; i < myDistance6.size(); i++) {
				distance6 += Double.parseDouble(myDistance6.get(i));
			}
			for (int i = 0; i < myDistance7.size(); i++) {
				distance7 += Double.parseDouble(myDistance7.get(i));
			}
			for (int i = 0; i < myDistance8.size(); i++) {
				distance8 += Double.parseDouble(myDistance8.get(i));
			}
			for (int i = 0; i < myDistance9.size(); i++) {
				distance9 += Double.parseDouble(myDistance9.get(i));
			}
			for (int i = 0; i < myDistance10.size(); i++) {
				distance10 += Double.parseDouble(myDistance10.get(i));
			}
			for (int i = 0; i < myDistance11.size(); i++) {
				distance11 += Double.parseDouble(myDistance11.get(i));
			}
			for (int i = 0; i < myDistance12.size(); i++) {
				distance12 += Double.parseDouble(myDistance12.get(i));
			}
			////////////////////////////////
			for (int i = 0; i < myDistanceDay1.size(); i++) {
				distanceDay1 += Double.parseDouble(myDistanceDay1.get(i));
			}
			for (int i = 0; i < myDistanceDay2.size(); i++) {
				distanceDay2 += Double.parseDouble(myDistanceDay2.get(i));
			}
			for (int i = 0; i < myDistanceDay3.size(); i++) {
				distanceDay3 += Double.parseDouble(myDistanceDay3.get(i));
			}
			for (int i = 0; i < myDistanceDay4.size(); i++) {
				distanceDay4 += Double.parseDouble(myDistanceDay4.get(i));
			}
			for (int i = 0; i < myDistanceDay5.size(); i++) {
				distanceDay5 += Double.parseDouble(myDistanceDay5.get(i));
			}
			for (int i = 0; i < myDistanceDay6.size(); i++) {
				distanceDay6 += Double.parseDouble(myDistanceDay6.get(i));
			}
			for (int i = 0; i < myDistanceDay7.size(); i++) {
				distanceDay7 += Double.parseDouble(myDistanceDay7.get(i));
			}
			for (int i = 0; i < myDistanceDay8.size(); i++) {
				distanceDay8 += Double.parseDouble(myDistanceDay8.get(i));
			}
			for (int i = 0; i < myDistanceDay9.size(); i++) {
				distanceDay9 += Double.parseDouble(myDistanceDay9.get(i));
			}
			for (int i = 0; i < myDistanceDay10.size(); i++) {
				distanceDay10 += Double.parseDouble(myDistanceDay10.get(i));
			}
			for (int i = 0; i < myDistanceDay11.size(); i++) {
				distanceDay11 += Double.parseDouble(myDistanceDay11.get(i));
			}
			for (int i = 0; i < myDistanceDay12.size(); i++) {
				distanceDay12 += Double.parseDouble(myDistanceDay12.get(i));
			}
			for (int i = 0; i < myDistanceDay13.size(); i++) {
				distanceDay13 += Double.parseDouble(myDistanceDay13.get(i));
			}
			for (int i = 0; i < myDistanceDay14.size(); i++) {
				distanceDay14 += Double.parseDouble(myDistanceDay14.get(i));
			}
			for (int i = 0; i < myDistanceDay15.size(); i++) {
				distanceDay15 += Double.parseDouble(myDistanceDay15.get(i));
			}
			for (int i = 0; i < myDistanceDay16.size(); i++) {
				distanceDay16 += Double.parseDouble(myDistanceDay16.get(i));
			}
			for (int i = 0; i < myDistanceDay17.size(); i++) {
				distanceDay17 += Double.parseDouble(myDistanceDay17.get(i));
			}
			for (int i = 0; i < myDistanceDay18.size(); i++) {
				distanceDay18 += Double.parseDouble(myDistanceDay18.get(i));
			}
			for (int i = 0; i < myDistanceDay19.size(); i++) {
				distanceDay19 += Double.parseDouble(myDistanceDay19.get(i));
			}
			for (int i = 0; i < myDistanceDay20.size(); i++) {
				distanceDay20 += Double.parseDouble(myDistanceDay20.get(i));
			}
			for (int i = 0; i < myDistanceDay21.size(); i++) {
				distanceDay21 += Double.parseDouble(myDistanceDay21.get(i));
			}
			for (int i = 0; i < myDistanceDay22.size(); i++) {
				distanceDay22 += Double.parseDouble(myDistanceDay22.get(i));
			}
			for (int i = 0; i < myDistanceDay23.size(); i++) {
				distanceDay23 += Double.parseDouble(myDistanceDay23.get(i));
			}
			for (int i = 0; i < myDistanceDay24.size(); i++) {
				distanceDay24 += Double.parseDouble(myDistanceDay24.get(i));
			}
			for (int i = 0; i < myDistanceDay25.size(); i++) {
				distanceDay25 += Double.parseDouble(myDistanceDay25.get(i));
			}
			for (int i = 0; i < myDistanceDay26.size(); i++) {
				distanceDay26 += Double.parseDouble(myDistanceDay26.get(i));
			}
			for (int i = 0; i < myDistanceDay27.size(); i++) {
				distanceDay27 += Double.parseDouble(myDistanceDay27.get(i));
			}
			for (int i = 0; i < myDistanceDay28.size(); i++) {
				distanceDay28 += Double.parseDouble(myDistanceDay28.get(i));
			}
			for (int i = 0; i < myDistanceDay29.size(); i++) {
				distanceDay29 += Double.parseDouble(myDistanceDay29.get(i));
			}
			for (int i = 0; i < myDistanceDay30.size(); i++) {
				distanceDay30 += Double.parseDouble(myDistanceDay30.get(i));
			}
			for (int i = 0; i < myDistanceDay31.size(); i++) {
				distanceDay31 += Double.parseDouble(myDistanceDay31.get(i));
			}

			respData.put("month1", distance1 / 1000);
			respData.put("month2", distance2 / 1000);
			respData.put("month3", distance3 / 1000);
			respData.put("month4", distance4 / 1000);
			respData.put("month5", distance5 / 1000);
			respData.put("month6", distance6 / 1000);
			respData.put("month7", distance7 / 1000);
			respData.put("month8", distance8 / 1000);
			respData.put("month9", distance9 / 1000);
			respData.put("month10", distance10 / 1000);
			respData.put("month11", distance11 / 1000);
			respData.put("month12", distance12 / 1000);
			
			respData.put("day1", distanceDay1 / 1000);
			respData.put("day2", distanceDay2 / 1000);
			respData.put("day3", distanceDay3 / 1000);
			respData.put("day4", distanceDay4 / 1000);
			respData.put("day5", distanceDay5 / 1000);
			respData.put("day6", distanceDay6 / 1000);
			respData.put("day7", distanceDay7 / 1000);
			respData.put("day8", distanceDay8 / 1000);
			respData.put("day9", distanceDay9 / 1000);
			respData.put("day10", distanceDay10 / 1000);
			respData.put("day11", distanceDay11 / 1000);
			respData.put("day12", distanceDay12 / 1000);
			respData.put("day13", distanceDay13 / 1000);
			respData.put("day14", distanceDay14 / 1000);
			respData.put("day15", distanceDay15 / 1000);
			respData.put("day16", distanceDay16 / 1000);
			respData.put("day17", distanceDay17 / 1000);
			respData.put("day18", distanceDay18 / 1000);
			respData.put("day19", distanceDay19 / 1000);
			respData.put("day20", distanceDay20 / 1000);
			respData.put("day21", distanceDay21 / 1000);
			respData.put("day22", distanceDay22 / 1000);
			respData.put("day23", distanceDay23 / 1000);
			respData.put("day24", distanceDay24 / 1000);
			respData.put("day25", distanceDay25 / 1000);
			respData.put("day26", distanceDay26 / 1000);
			respData.put("day27", distanceDay27 / 1000);
			respData.put("day28", distanceDay28 / 1000);
			respData.put("day29", distanceDay29 / 1000);
			respData.put("day30", distanceDay30 / 1000);
			respData.put("day31", distanceDay31 / 1000);

			strResponse = new Gson().toJson(respData);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
				rs.close();
				preparedStatement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return strResponse;
	}

	public static double distance(double lat1, double lat2, double lon1, double lon2, double el1, double el2) {

		final int R = 6371; // Radius of the earth

		double latDistance = Math.toRadians(lat2 - lat1);
		double lonDistance = Math.toRadians(lon2 - lon1);
		double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(Math.toRadians(lat1))
				* Math.cos(Math.toRadians(lat2)) * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double distance = R * c * 1000; // convert to meters

		double height = el1 - el2;

		distance = Math.pow(distance, 2) + Math.pow(height, 2);
		return Math.sqrt(distance);
	}
	
	public String getReportChooseHealthy(Connection connection, int selectCane, String searchStartDate,
			String searchEndDate) {
		PreparedStatement preparedStatement = null;
		String strResponse = "";
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();

		Double distance1 = 0.0;

		ArrayList<String> myLatitude1 = new ArrayList<String>();
		ArrayList<String> myLongitude1 = new ArrayList<String>();
		ArrayList<String> myDistance1 = new ArrayList<String>();

		HashMap<String, Object> respData = new HashMap<String, Object>();
		int j = 1;
		try {
			sql.append(" SELECT LONGITUDE,LATITUDE,ucc.WEIGHT,gh.CREATE_DATE ");
			sql.append(" FROM GPS g ");
			sql.append(" LEFT JOIN GPS_HISTORY gh ON gh.GPS_ID = g.GPS_ID ");
			sql.append(" LEFT JOIN user_conect_cane ucc on ucc.CANE_ID = g.CANE_ID ");
			sql.append(" WHERE gh.CREATE_DATE between ? and ? ");
			sql.append(" AND g.CANE_ID = ? ");
			sql.append(" order by gh.CREATE_DATE asc ");
			preparedStatement = connection.prepareStatement(sql.toString());
			preparedStatement.setString(1, searchStartDate.concat(" 00:00:00"));
			preparedStatement.setString(2, searchEndDate.concat(" 23:59:59"));
			preparedStatement.setInt(3, selectCane);
			rs = preparedStatement.executeQuery();
			while ((rs != null) && (rs.next())) {

				myLatitude1.add(rs.getString("LONGITUDE"));
				myLongitude1.add(rs.getString("LATITUDE"));
				respData.put("WEIGHT", rs.getString("WEIGHT"));

			}
			
			for (int i = 0; i < myLatitude1.size(); i++) {
				if (i < myLatitude1.size() - 1 && myLatitude1.size() % 2 == 1) {
					myDistance1.add(Double.toString(distance(Double.parseDouble(myLatitude1.get(i)),
							Double.parseDouble(myLatitude1.get(j)), Double.parseDouble(myLongitude1.get(i)),
							Double.parseDouble(myLongitude1.get(j)), 0, 0)));
					j++;
				} else if (i < myLatitude1.size() - 1 && myLatitude1.size() % 2 == 0) {
					myDistance1.add(Double.toString(distance(Double.parseDouble(myLatitude1.get(i)),
							Double.parseDouble(myLatitude1.get(j)), Double.parseDouble(myLongitude1.get(i)),
							Double.parseDouble(myLongitude1.get(j)), 0, 0)));
					j++;
				}
			}
			j = 1;

			for (int i = 0; i < myDistance1.size(); i++) {
				distance1 += Double.parseDouble(myDistance1.get(i));
			}

			respData.put("day1", distance1 / 1000);

			strResponse = new Gson().toJson(respData);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
				rs.close();
				preparedStatement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return strResponse;
	}

	
}
