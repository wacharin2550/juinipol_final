package bus;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class RegisterUsernameManagement {
	
	public Boolean registerUser(Connection connection,String name,String lastName,String userName,String password)throws Exception {
		PreparedStatement preparedStatement = null;
		StringBuilder strBuilder = new StringBuilder();
		int numRow = 0;
		Boolean status = false;
		try {
			strBuilder.append(" INSERT INTO USERS ");
			strBuilder.append(" (NAME, LASTNAME, USERNAME, PASSWORDS) ");
			strBuilder.append(" VALUES(?, ?, ?, ?); ");
			preparedStatement = connection.prepareStatement(strBuilder.toString());
			preparedStatement.setString(1, name);
			preparedStatement.setString(2, lastName);
			preparedStatement.setString(3, userName);
			preparedStatement.setString(4, password);
			
			numRow = preparedStatement.executeUpdate();
			if (numRow>0) {
				status = true;
			}else {
				status = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}
}
