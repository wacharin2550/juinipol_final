package bus;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import com.google.gson.Gson;
import com.sun.media.sound.InvalidDataException;

import bean.LineBean;
import bean.UserConectionCaneBean;
import bean.UserNameBean;

public class InsertLineManagement {

	public String getLineName(Connection connection, int userId) {
		PreparedStatement preparedStatement = null;
		String strResponse = "";
		ResultSet rs = null;

		StringBuilder sql = new StringBuilder();
		ArrayList<LineBean> alist = new ArrayList<LineBean>();
		try {

			sql.append(" SELECT LINE_ID, LINE_NAME, LINE_STATUS, LINE_TOKEN ");
			sql.append(" FROM LINE ");
			sql.append(" WHERE USERS_ID = ? ");
			preparedStatement = connection.prepareStatement(sql.toString());
			preparedStatement.setInt(1, userId);
			rs = preparedStatement.executeQuery();
			while ((rs != null) && (rs.next())) {
				LineBean uBean = new LineBean();
				uBean.setLineName(rs.getString("LINE_NAME"));
				uBean.setLineId(rs.getString("LINE_ID"));
				uBean.setLineStatus(rs.getString("LINE_STATUS"));
				uBean.setLineToken(rs.getString("LINE_TOKEN"));
				alist.add(uBean);
			}
			strResponse = new Gson().toJson(alist);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
				rs.close();
				preparedStatement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return strResponse;
	}

	public String getEditLineName(Connection connection, String userId) {
		PreparedStatement preparedStatement = null;
		String strResponse = "";
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();
		ArrayList<LineBean> alist = new ArrayList<LineBean>();

		try {

			sql.append(" SELECT LINE_NAME, LINE_TOKEN ");
			sql.append(" FROM LINE ");
			sql.append(" WHERE LINE_ID = ? ; ");
			preparedStatement = connection.prepareStatement(sql.toString());
			preparedStatement.setString(1, userId);
			rs = preparedStatement.executeQuery();
			while ((rs != null) && (rs.next())) {
				LineBean uBean = new LineBean();
				uBean.setLineName(rs.getString("LINE_NAME"));
				uBean.setLineToken(rs.getString("LINE_TOKEN"));
				alist.add(uBean);
			}
			strResponse = new Gson().toJson(alist);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
				rs.close();
				preparedStatement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return strResponse;
	}

	public int insertLine(Connection connection, int userId, LineBean[] lineName) throws Exception {
		PreparedStatement stmt = null;
		StringBuilder strBuilder = new StringBuilder();
		int insertedTotal = 0;
		try {
			strBuilder.append(" INSERT INTO LINE ");
			strBuilder.append(" (LINE_NAME, LINE_STATUS, LINE_TOKEN, USERS_ID) ");
			strBuilder.append(" VALUES(?,1,?,?);");
			stmt = connection.prepareStatement(strBuilder.toString());
			for (LineBean bean : lineName) {
				System.out.println(bean.getLineName());
				System.out.println(bean.getLineToken());
				stmt.setString(1, bean.getLineName());
				stmt.setString(2, bean.getLineToken());
				stmt.setInt(3, userId);
				insertedTotal = stmt.executeUpdate();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			connection.close();
		}
		return insertedTotal;
	}

	public int updateLine(Connection connection, String lineId) throws Exception {
		PreparedStatement stmt = null;
		StringBuilder strBuilder = new StringBuilder();
		int insertedTotal = 0;
		try {
			strBuilder.append(" UPDATE LINE ");
			strBuilder.append(" SET  LINE_STATUS = 0 ");
			strBuilder.append(" WHERE LINE_ID = ? ");
			stmt = connection.prepareStatement(strBuilder.toString());
			stmt.setString(1, lineId);
			insertedTotal = stmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			connection.close();
		}
		return insertedTotal;
	}

	public int updateStartLine(Connection connection, String lineId) throws Exception {
		PreparedStatement stmt = null;
		StringBuilder strBuilder = new StringBuilder();
		int insertedTotal = 0;
		try {
			strBuilder.append(" UPDATE LINE ");
			strBuilder.append(" SET LINE_STATUS=1 ");
			strBuilder.append(" WHERE LINE_ID = ? ");
			stmt = connection.prepareStatement(strBuilder.toString());
			stmt.setString(1, lineId);
			insertedTotal = stmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			connection.close();
		}
		return insertedTotal;
	}

	public int editLine(Connection connection, String lineId, String lineName, String lineToken) throws Exception {
		PreparedStatement stmt = null;
		StringBuilder strBuilder = new StringBuilder();
		int insertedTotal = 0;
		try {
			strBuilder.append(" UPDATE line ");
			strBuilder.append(" SET LINE_NAME = ?, LINE_TOKEN = ? ");
			strBuilder.append(" WHERE LINE_ID = ? ; ");
			stmt = connection.prepareStatement(strBuilder.toString());
			stmt.setString(1, lineName);
			stmt.setString(2, lineToken);
			stmt.setString(3, lineId);
			insertedTotal = stmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			connection.close();
		}
		return insertedTotal;
	}
}
