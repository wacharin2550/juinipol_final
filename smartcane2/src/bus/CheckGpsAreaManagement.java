package bus;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.google.gson.Gson;

import bean.LocationAreaBean;

public class CheckGpsAreaManagement {
	public String getSmartCaneRadius(Connection connection,int caneID,String userId) {
		PreparedStatement preparedStatement= null;
		String strResponse = "";
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();
		String locationJson = null;
		ArrayList<LocationAreaBean>alist = new ArrayList<LocationAreaBean>();
		try {

			sql.append(" SELECT LOCATION_AREA_ID,la.CANE_USER_ID,ucc.CANE_ID,  LONGITUDE, LATITUDE,  "); 
			sql.append(" RADIUS_DISTANCE, LOCATION_STATUS ");
			sql.append(" FROM LOCATION_AREA la "); 
			sql.append(" LEFT JOIN USER_CONECT_CANE ucc ON ucc.CANE_USER_ID = la.CANE_USER_ID "); 
			sql.append(" WHERE ucc.CANE_ID = ? AND ucc.USERS_ID = ? ");
			
			preparedStatement = connection.prepareStatement(sql.toString());
			preparedStatement.setInt(1, caneID);
			preparedStatement.setString(2, userId);
			rs = preparedStatement.executeQuery();
			
			
			while((rs!=null) && (rs.next())) {
				String longitudeJson = "{\"longitude\":\""+rs.getString("LONGITUDE")+"\",";
				String latitudeJson = "\"latitude\":\""+rs.getString("LATITUDE")+"\",";
				String radiusJson = "\"radius\":\""+rs.getString("RADIUS_DISTANCE")+"\",";
				String caneIdJson = "\"caneId\":\""+rs.getString("CANE_ID")+"\"}";
				locationJson = longitudeJson.concat(latitudeJson).concat(radiusJson).concat(caneIdJson);
			}
		}catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				preparedStatement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return locationJson;
	}
}
