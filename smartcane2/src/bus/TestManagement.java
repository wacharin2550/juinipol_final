package bus;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpSession;

public class TestManagement {
	
	
	public boolean Login(Connection connection,String user,String password,HttpSession session) {
		PreparedStatement preparedStatement= null;
		StringBuilder strBuilder = new StringBuilder();
		ResultSet rs = null;
		boolean issucsess = false;
		try {
			strBuilder.append(" SELECT USERNAME, PASSWORDS, NAME ,LASTNAME,USERS_ID ");
			strBuilder.append(" FROM USERS ");
			strBuilder.append(" WHERE USERNAME = ? AND PASSWORDS = ?; ");
			preparedStatement = connection.prepareStatement(strBuilder.toString());
		
			preparedStatement.setString(1, user);
			preparedStatement.setString(2, password);
			
			rs = preparedStatement.executeQuery();
			
			if (rs.next()) {
				issucsess = true;
				session.setAttribute("userId",  rs.getString("USERS_ID"));	
				session.setAttribute("userName", rs.getString("NAME").concat(" ").concat(rs.getString("LASTNAME")));	
				session.setAttribute("statusOnline", true);	
			}else {
				issucsess = false;
			}
			
			
		} catch (SQLException e1) {
			e1.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return issucsess;
	}
}
