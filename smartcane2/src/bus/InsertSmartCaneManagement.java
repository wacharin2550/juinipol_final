package bus;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import com.google.gson.Gson;
import com.sun.media.sound.InvalidDataException;

import bean.UserConectionCaneBean;
import bean.UserNameBean;

public class InsertSmartCaneManagement {
	
	
	public String getSmartCaneName(Connection connection,int userId) {
		PreparedStatement preparedStatement= null;
		String strResponse = "";
		ResultSet rs = null;
		
		StringBuilder sql = new StringBuilder();
		ArrayList<UserConectionCaneBean>alist = new ArrayList<UserConectionCaneBean>();
		try {

			sql.append(" SELECT  CANE_USER_ID,u.USERS_ID, c.CANE_ID, CANE_NAME,c.CANE_TOKEN, CANE_STATUS,ucc.AGE,ucc.HEIGHT,ucc.WEIGHT  "); 
			sql.append(" FROM USER_CONECT_CANE ucc ");
			sql.append(" LEFT JOIN USERS u ON u.USERS_ID = ucc.USERS_ID "); 
			sql.append(" LEFT JOIN CANE c ON c.CANE_ID = ucc.CANE_ID "); 
			sql.append(" WHERE u.USERS_ID = ? ");
			preparedStatement = connection.prepareStatement(sql.toString());
			preparedStatement.setInt(1, userId);
			rs = preparedStatement.executeQuery();
			while((rs!=null) && (rs.next())) {
				UserConectionCaneBean uBean = new UserConectionCaneBean();
				uBean.setCaneName(rs.getString("CANE_NAME"));
				uBean.setCaneId(rs.getString("CANE_ID"));
				uBean.setCaneUserId(rs.getString("CANE_USER_ID"));
				uBean.setTokenID(rs.getString("CANE_TOKEN"));
				uBean.setCaneStatus(rs.getString("CANE_STATUS"));
				uBean.setAge(rs.getString("AGE"));
				uBean.setHeight(rs.getString("HEIGHT"));
				uBean.setWeight(rs.getString("WEIGHT"));
				alist.add(uBean);
			}
			strResponse = new Gson().toJson(alist);
		}catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
				rs.close();
				preparedStatement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return strResponse;
	}
	
	public Boolean checkTokenSmartCane(Connection connection,String caneToken) {
		PreparedStatement preparedStatement= null;
		ResultSet rs = null;
		Boolean checkAlready = false;
		StringBuilder sql = new StringBuilder();
		try {

			sql.append(" SELECT CANE_TOKEN  "); 
			sql.append(" FROM CANE ");
			sql.append(" WHERE CANE_TOKEN = ? "); 
			
			preparedStatement = connection.prepareStatement(sql.toString());
			preparedStatement.setString(1, caneToken);
			rs = preparedStatement.executeQuery();
			if ((rs!=null) && (rs.next())) {
				checkAlready = true;
			}else {
				checkAlready = false;
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		} 
		return checkAlready;
	}
	public Boolean checkNameForUser(Connection connection,String caneName,int userId) {
		PreparedStatement preparedStatement= null;
		ResultSet rs = null;
		Boolean checkAlready = false;
		StringBuilder sql = new StringBuilder();
		try {

			sql.append(" SELECT CANE_USER_ID "); 
			sql.append(" FROM user_conect_cane ");
			sql.append(" WHERE USERS_ID = ? and CANE_NAME= ? ; "); 
			
			preparedStatement = connection.prepareStatement(sql.toString());
			System.out.println("caneName : "+caneName);
			preparedStatement.setInt(1, userId);
			preparedStatement.setString(2, caneName);
			rs = preparedStatement.executeQuery();
			if ((rs!=null) && (rs.next())) {
				checkAlready = true;
				System.out.println("checkAlready : "+checkAlready);
			}else {
				checkAlready = false;
				System.out.println("checkAlready : "+checkAlready);
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		} 
		return checkAlready;
	}
	
	public HashMap<String, Object>  insertCane(Connection connection, 
			int userId, UserConectionCaneBean[] smartCaneName) throws Exception{
		PreparedStatement stmt = null;
		HashMap<String, Object> resp = new HashMap<String, Object>();
		StringBuilder strBuilder = new StringBuilder();
		int insertedTotal =  0;
		try {	
			for (UserConectionCaneBean bean: smartCaneName) {
				if (!checkNameForUser(connection, bean.getSmartCaneName(), userId)) {
					if (checkTokenSmartCane(connection, bean.getSmartCaneToken())) {
						insertConectionCaneToUser(connection, userId, bean.getSmartCaneName(), bean.getSmartCaneToken());
						System.out.println("IF");
					}else {
						strBuilder.append(" INSERT INTO CANE ");		
						strBuilder.append(" (CANE_TOKEN) ");		
						strBuilder.append(" VALUES( ? ); ");		
						stmt = connection.prepareStatement(strBuilder.toString());		
						stmt.setString(1, bean.getSmartCaneToken());
						insertedTotal = stmt.executeUpdate();
						insertConectionCaneToUser(connection, userId, bean.getSmartCaneName(), bean.getSmartCaneToken());
						System.out.println("ELSE");
					}
				}else {
					resp.put("isSuccess", false);
				}
			}
			System.out.println(insertedTotal);
			if (insertedTotal == smartCaneName.length) {
				resp.put("isSuccess", true);
			}
			
		}  catch (Exception e) {
			e.printStackTrace();
		} finally {
			connection.close();
		}
		return resp;
	}
	
	public void  insertConectionCaneToUser(Connection connection, 
			int userId,String caneName,String caneToken) throws Exception{
		PreparedStatement stmt = null;
		HashMap<String, Object> resp = new HashMap<String, Object>();
		StringBuilder strBuilder = new StringBuilder();
		try {
			strBuilder.append(" INSERT INTO USER_CONECT_CANE ");		
			strBuilder.append(" (USERS_ID, CANE_ID, CANE_NAME, CANE_STATUS) ");		
			strBuilder.append(" VALUES( ?,(SELECT CANE_ID FROM CANE c WHERE c.CANE_TOKEN =  ?),?,1 ); ");		
			stmt = connection.prepareStatement(strBuilder.toString());			
			stmt.setInt(1, userId);
			stmt.setString(2, caneToken);
			stmt.setString(3, caneName);
			int insertedTotal = stmt.executeUpdate();
			if (insertedTotal>0) {
				resp.put("isSuccess", true);
			}else {
				resp.put("isSuccess", false);
			}
			
			
		}  catch (Exception e) {
			e.printStackTrace();
		} finally {
			connection.close();
		}
	}
	
	public int updateCane(Connection connection, String userCaneId)throws Exception {
		PreparedStatement stmt = null;
		StringBuilder strBuilder = new StringBuilder();
		int insertedTotal = 0;
		try {
			strBuilder.append(" UPDATE user_conect_cane ");
			strBuilder.append(" SET  CANE_STATUS  = 0 ");
			strBuilder.append(" WHERE CANE_USER_ID = ? ");
			stmt = connection.prepareStatement(strBuilder.toString());
			stmt.setString(1, userCaneId);
			insertedTotal = stmt.executeUpdate();
		
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			connection.close();
		}
		return insertedTotal;
	}
	
	public int updateStartCane(Connection connection, String userCaneId)throws Exception {
		PreparedStatement stmt = null;
		StringBuilder strBuilder = new StringBuilder();
		int insertedTotal = 0;
		try {
			strBuilder.append(" UPDATE user_conect_cane ");
			strBuilder.append(" SET  CANE_STATUS = 1 ");
			strBuilder.append(" WHERE CANE_USER_ID = ? ");
			stmt = connection.prepareStatement(strBuilder.toString());
			stmt.setString(1, userCaneId);
			insertedTotal = stmt.executeUpdate();
		
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			connection.close();
		}
		return insertedTotal;
	}
	
	public int  addInfoCane(Connection connection,String userCaneId,String age,String height,String weight,String caneName) throws Exception{
		PreparedStatement stmt = null;
		StringBuilder strBuilder = new StringBuilder();
		int insertedTotal = 0 ;
		try {
			strBuilder.append(" UPDATE user_conect_cane ");		
			strBuilder.append(" SET  AGE = ?, WEIGHT = ?, HEIGHT = ? ,CANE_NAME = ?");		
			strBuilder.append(" WHERE CANE_USER_ID = ? ; ");		
			stmt = connection.prepareStatement(strBuilder.toString());			
			stmt.setString(1, age);
			stmt.setString(2, weight);
			stmt.setString(3, height);
			stmt.setString(4, caneName);
			stmt.setString(5, userCaneId);
			insertedTotal = stmt.executeUpdate();
			
		}  catch (Exception e) {
			e.printStackTrace();
		} finally {
			connection.close();
		}
		return insertedTotal;
	}
	
	public String getSmartCaneInfo(Connection connection,int userCaneId) {
		PreparedStatement preparedStatement= null;
		String strResponse = "";
		ResultSet rs = null;
		
		StringBuilder sql = new StringBuilder();
		ArrayList<UserConectionCaneBean>alist = new ArrayList<UserConectionCaneBean>();
		try {

			sql.append(" SELECT AGE, WEIGHT, HEIGHT,CANE_NAME  "); 
			sql.append(" FROM user_conect_cane ");
			sql.append(" WHERE CANE_USER_ID=?; "); 
			preparedStatement = connection.prepareStatement(sql.toString());
			preparedStatement.setInt(1, userCaneId);
			rs = preparedStatement.executeQuery();
			while((rs!=null) && (rs.next())) {
				UserConectionCaneBean uBean = new UserConectionCaneBean();
				uBean.setAge(rs.getString("AGE"));
				uBean.setHeight(rs.getString("HEIGHT"));
				uBean.setWeight(rs.getString("WEIGHT"));
				uBean.setCaneName(rs.getString("CANE_NAME"));
				alist.add(uBean);
			}
			strResponse = new Gson().toJson(alist);
		}catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
				rs.close();
				preparedStatement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return strResponse;
	}
}
