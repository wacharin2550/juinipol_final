package connection;


import java.sql.Connection;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.sql.DataSource;

/**
 * Servlet implementation class connection
 */
@WebServlet("/connection")
public class connectionMysql extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public connectionMysql() {
        super();
    }

    public Connection connection() {
    	Connection connect = null;

		try {
			 Context initContext = new InitialContext();
			 Context envContext = (Context)initContext.lookup("java:/comp/env");
			 DataSource jdbc_ds = (DataSource)envContext.lookup("jdbc/smartcane");
			 connect = jdbc_ds.getConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return connect;
	}

}
