package servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bus.InsertGpsManagement;
import connection.connectionMysql;

@WebServlet("/InsertGpsServlet")
public class InsertGpsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	connectionMysql connection = new connectionMysql();

	public InsertGpsServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			String method = request.getParameter("method");
			String jsonFromNodeMcu = request.getParameter("jsonCurrent");
			
			if ("insertGps".equals(method)) {
				InsertGpsManagement iManagement = new InsertGpsManagement();
				URL url = new URL("https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyD8G9vmTJl3OsJP9CqoW9QMCbGweJA473s");
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setDoOutput(true);
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Content-Type", "application/json");
				OutputStream os = conn.getOutputStream();
				os.write(jsonFromNodeMcu.getBytes());
				os.flush();

				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

				String output;
				String strbuild = "";
				while ((output = br.readLine()) != null) {
					strbuild = strbuild + output;
				}
				JsonReader jsonReader = Json.createReader(new StringReader(strbuild));
				JsonObject reply = jsonReader.readObject();

				String latitude = reply.getJsonObject("location").getJsonNumber("lat").toString();
				String longitude = reply.getJsonObject("location").getJsonNumber("lng").toString();
				conn.disconnect();
				String caneToken = request.getParameter("caneToken");
				Boolean success = iManagement.insertGps(connection.connection(), caneToken, latitude, longitude);


				String location = iManagement.getLocationArea(connection.connection(), caneToken);
				JsonReader jsonReaderLocation = Json.createReader(new StringReader(location));
				JsonArray replyLocation = jsonReaderLocation.readArray();
				for (int i = 0; i < replyLocation.size(); i++) {
					String latitudeLocation = replyLocation.getJsonObject(i).getString("latitude").toString();
					String longitudeLocation = replyLocation.getJsonObject(i).getString("longitude").toString();
					String radius = replyLocation.getJsonObject(i).getString("radius").toString();
					String caneId = replyLocation.getJsonObject(i).getString("caneId").toString();
					String userIdForLine = replyLocation.getJsonObject(i).getString("userIdForLine").toString();
					String locationAreaId = replyLocation.getJsonObject(i).getString("locationAreaId").toString();
					if (distance(Double.parseDouble(latitude), Double.parseDouble(latitudeLocation), Double.parseDouble(longitude),Double.parseDouble(longitudeLocation), 0,
							0) > Double.parseDouble(radius)) {
						iManagement.sendToLindRadius(connection.connection(), caneId, userIdForLine, latitude, longitude, locationAreaId);
					} else {
						System.out.println(2);
					}
				}
				
				
				response.setCharacterEncoding("UTF8");
				response.getWriter().print(success);
			} else {
				System.out.println("Not Method Post");
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				connection.connection().close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public static double distance(double lat1, double lat2, double lon1, double lon2, double el1, double el2) {

		final int R = 6371; // Radius of the earth

		double latDistance = Math.toRadians(lat2 - lat1);
		double lonDistance = Math.toRadians(lon2 - lon1);
		double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(Math.toRadians(lat1))
				* Math.cos(Math.toRadians(lat2)) * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double distance = R * c * 1000; // convert to meters

		double height = el1 - el2;

		distance = Math.pow(distance, 2) + Math.pow(height, 2);
		System.out.println("Distance : "+Math.sqrt(distance));
		return Math.sqrt(distance);
	}

}
