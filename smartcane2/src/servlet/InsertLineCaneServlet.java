package servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import bean.LineCaneBean;
import bus.InsertLineCaneManagement;
import connection.connectionMysql;

@WebServlet("/InsertLineCaneServlet")
public class InsertLineCaneServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	connectionMysql connection = new connectionMysql();    
    public InsertLineCaneServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String method = request.getParameter("method");
		HttpSession session = request.getSession();
		String userID = (String)session.getAttribute("userId");
		String strJsonResp = "";
		try {
			InsertLineCaneManagement insertLineCane = new InsertLineCaneManagement();
			if ("loadLineCane".equals(method)) {
				strJsonResp =  insertLineCane.getLineCaneName(connection.connection(),Integer.parseInt(userID));
			}
		} catch (Exception e) {
			System.out.println(e);
		}finally {
			try {
				connection.connection().close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		response.setCharacterEncoding("UTF8");
		response.getWriter().print(strJsonResp);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String userID = (String)session.getAttribute("userId");
		String method = request.getParameter("method");
		HashMap<String, Object> respData = new HashMap<String, Object>();
		Gson gson = new Gson();
		String strJsonResp = "";
		System.out.println(method + " : method");
		try {
			InsertLineCaneManagement insertLine = new InsertLineCaneManagement();
			if ("addNewLineCane".equals(method)) {
				String jsonNewLineCane = request.getParameter("newLineCane");
				LineCaneBean[] arrayLineCane = gson.fromJson(jsonNewLineCane,LineCaneBean[].class);
				respData.put("numRow", insertLine.insertLineCane(connection.connection(),Integer.parseInt(userID),arrayLineCane));
			}else if ("stopLineCane".equals(method)) {
				String lineCaneId = request.getParameter("lineCaneId");
				respData.put("numRow", insertLine.updateLineCane(connection.connection(),lineCaneId));
			}else if ("startLineCane".equals(method)) {
				String lineCaneId = request.getParameter("lineCaneId");
				respData.put("numRow", insertLine.updateStartLineCane(connection.connection(),lineCaneId));
			}else {
				System.out.println("Not Method Post");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				connection.connection().close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		strJsonResp = gson.toJson(respData);
		response.setCharacterEncoding("UTF8");
		response.getWriter().print(strJsonResp);
	}

}
