package servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bus.CurrentGpsManagement;
import connection.connectionMysql;

@WebServlet("/CurrentGpsServlet")
public class CurrentGpsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
      
	connectionMysql connection = new connectionMysql();
    public CurrentGpsServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String method = request.getParameter("method");
		HttpSession session = request.getSession();
		String userID = (String)session.getAttribute("userId");
		String userID2 = (String)session.getAttribute("userId2");
		String strJsonResp = "";
		try {
			CurrentGpsManagement gpsManagement = new CurrentGpsManagement();
			if ("loadGpsLocation".equals(method)) {
				String selectSmartCane = request.getParameter("selectCane");
				strJsonResp =  gpsManagement.getGpsLocation(connection.connection(),Integer.parseInt(selectSmartCane),userID);
			}else {
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				connection.connection().close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		response.setCharacterEncoding("UTF8");
		response.getWriter().print(strJsonResp);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	}

}
