package servlet;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bus.EmergencyLineManagement;
import connection.connectionMysql;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

@WebServlet("/EmergencyLineServlet")
public class EmergencyLineServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String URL_LINE_API = "https://notify-api.line.me/api/notify";
	connectionMysql connection = new connectionMysql();
	
	public EmergencyLineServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		String userId = (String)session.getAttribute("userId");
		String method = request.getParameter("method");
		String jsonFromNodeMcu = request.getParameter("jsonCurrent");
		String caneToken = request.getParameter("caneToken");
		try {
			if ("emergencyButton".equals(method)) {
				EmergencyLineManagement iManagement = new EmergencyLineManagement();
				URL url = new URL("https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyD8G9vmTJl3OsJP9CqoW9QMCbGweJA473s");
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setDoOutput(true);
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Content-Type", "application/json");
				OutputStream os = conn.getOutputStream();
				os.write(jsonFromNodeMcu.getBytes());
				os.flush();
				
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

				String output;
				String strbuild = "";
				while ((output = br.readLine()) != null) {
					strbuild = strbuild + output;
				}
				JsonReader jsonReader = Json.createReader(new StringReader(strbuild));
				JsonObject reply = jsonReader.readObject();
				
				String latitude = reply.getJsonObject("location").getJsonNumber("lat").toString();
				String longitude = reply.getJsonObject("location").getJsonNumber("lng").toString();
				conn.disconnect();
				iManagement.sendToLindEmergency(connection.connection(), caneToken, userId, latitude, longitude);
				
			}
			
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	private static HttpURLConnection preparedProperty(final String TOKEN) throws MalformedURLException, IOException {
		URL url = new URL(URL_LINE_API);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		StringBuilder Authorization = new StringBuilder("Bearer ");
		conn.setDoOutput(true);
		conn.setDoInput(true);
		conn.setInstanceFollowRedirects(false);
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		conn.setRequestProperty("Authorization", Authorization.append(TOKEN).toString());
		conn.setUseCaches(false);
		return conn;
	}

	// set body parameter for http request
	private static String preparedParameter(LineParameter l) {
		StringBuilder urlParameters = new StringBuilder();
		urlParameters.append("message=" + l.getMessage());
		if (l.getStickerId() > 0 && l.getStickerPackageId() > 0) {
			urlParameters.append("&stickerPackageId=" + l.getStickerId());
			urlParameters.append("&stickerId=" + l.getStickerPackageId());
		}
		return urlParameters.toString();
	}

	// send data via http request
	public static int sendData(LineParameter lineData, final String TOKEN) throws IOException {
		String urlParameters = preparedParameter(lineData);
		byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
		int postDataLength = postData.length;
		int statusCode = 500;
		HttpURLConnection conn = preparedProperty(TOKEN);
		conn.setRequestProperty("charset", "utf-8");
		conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
		conn.setUseCaches(false);
		try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
			wr.write(postData);
			wr.flush();
		}
		if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
			statusCode = conn.getResponseCode();
			if (statusCode != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}
		}
		conn.disconnect();
		return statusCode;
	}

}
