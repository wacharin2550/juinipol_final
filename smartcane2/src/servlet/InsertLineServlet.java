package servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import bean.LineBean;
import bus.InsertLineManagement;
import connection.connectionMysql;

@WebServlet("/InsertLineServlet")
public class InsertLineServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	connectionMysql connection = new connectionMysql();    
    public InsertLineServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String method = request.getParameter("method");
		HttpSession session = request.getSession();
		String userID = (String)session.getAttribute("userId");
		String strJsonResp = "";
		try {
			InsertLineManagement insertLine = new InsertLineManagement();
			if ("loadLine".equals(method)) {
				strJsonResp =  insertLine.getLineName(connection.connection(),Integer.parseInt(userID));
			}else if ("loadEditLine".equals(method)) {
				String lineId = request.getParameter("lineId");
				strJsonResp =  insertLine.getEditLineName(connection.connection(),lineId);
			}
		} catch (Exception e) {
			System.out.println(e);
		}finally {
			try {
				connection.connection().close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		response.setCharacterEncoding("UTF8");
		response.getWriter().print(strJsonResp);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String userID = (String)session.getAttribute("userId");
		String method = request.getParameter("method");
		HashMap<String, Object> respData = new HashMap<String, Object>();
		Gson gson = new Gson();
		String strJsonResp = "";
		System.out.println(method + " : method");
		try {
			InsertLineManagement insertLine = new InsertLineManagement();
			if ("addNewLine".equals(method)) {
				String jsonNewLine = request.getParameter("newLine");
				LineBean[] arrayLine = gson.fromJson(jsonNewLine,LineBean[].class);
				respData.put("numRow", insertLine.insertLine(connection.connection(),Integer.parseInt(userID),arrayLine));
			}else if ("stopLine".equals(method)) {
				String lineId = request.getParameter("lineId");
				respData.put("numRow", insertLine.updateLine(connection.connection(),lineId));
			}else if ("startLine".equals(method)) {
				String lineId = request.getParameter("lineId");
				respData.put("numRow", insertLine.updateStartLine(connection.connection(),lineId));
			}else if ("editInfoLine".equals(method)) {
				String lineId = request.getParameter("lineId");
				String lineName = request.getParameter("lineName");
				String lineToken = request.getParameter("lineToken");
				respData.put("numRow", insertLine.editLine(connection.connection(), lineId, lineName, lineToken));
			}else {
				System.out.println("Not Method Post");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				connection.connection().close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		strJsonResp = gson.toJson(respData);
		response.setCharacterEncoding("UTF8");
		response.getWriter().print(strJsonResp);
	}

}
