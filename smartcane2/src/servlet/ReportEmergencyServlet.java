package servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bus.ReportEmergencyManagement;
import connection.connectionMysql;

@WebServlet("/ReportEmergencyServlet")
public class ReportEmergencyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
     connectionMysql connection = new connectionMysql();
    public ReportEmergencyServlet() {
        super();
    }
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String method = request.getParameter("method");
		String strJsonResp = "";
		try {
			ReportEmergencyManagement reEmergencyManagement = new ReportEmergencyManagement();
			if ("reportEmergency".equals(method)) {
				String selectCane = request.getParameter("selectCane");
				String yearSearch = request.getParameter("yearSearch");
				String monthSeach = request.getParameter("monthSeach");
				String yearSearchForMonth = request.getParameter("yearSearchForMonth");
				strJsonResp =  reEmergencyManagement.getEmergencyHistory(connection.connection(),Integer.parseInt(selectCane),yearSearch,monthSeach,yearSearchForMonth);
			}else  if ("reportChooseEmergency".equals(method)){
				String selectCane = request.getParameter("selectCane");
				String searchStartDate = request.getParameter("searchStartDate");
				String searchEndDate = request.getParameter("searchEndDate");
				strJsonResp =  reEmergencyManagement.getEmergencyChooseHistory(connection.connection(),Integer.parseInt(selectCane),searchStartDate,searchEndDate);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				connection.connection().close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		response.setCharacterEncoding("UTF8");
		response.getWriter().print(strJsonResp);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
