package servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bus.ReportUltrasonicManagement;
import connection.connectionMysql;

@WebServlet("/ReportUltrasonicServlet")
public class ReportUltrasonicServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
     connectionMysql connection = new connectionMysql();
    public ReportUltrasonicServlet() {
        super();
    }
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String method = request.getParameter("method");
		String strJsonResp = "";
		try {
			ReportUltrasonicManagement reportUltrasonicManagement = new ReportUltrasonicManagement();
			if ("reportUltrasonic".equals(method)) {
				String selectCane = request.getParameter("selectCane");
				System.out.println("selectCane : "+selectCane);
				strJsonResp =  reportUltrasonicManagement.getUltrasonicHistory(connection.connection(),Integer.parseInt(selectCane));
			}else {
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				connection.connection().close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		response.setCharacterEncoding("UTF8");
		response.getWriter().print(strJsonResp);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
