package servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bus.ReportLocationAlertManagement;
import connection.connectionMysql;

@WebServlet("/ReportLocationAlertServlet")
public class ReportLocationAlertServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
     connectionMysql connection = new connectionMysql();
    public ReportLocationAlertServlet() {
        super();
    }
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String method = request.getParameter("method");
		String strJsonResp = "";
		HttpSession session = request.getSession();
		String userId = (String)session.getAttribute("userId");
		try {
			ReportLocationAlertManagement reAlertManagement = new ReportLocationAlertManagement();
			if ("reportLocationAlert".equals(method)) {
				String selectCane = request.getParameter("selectCane");
				String yearSearch = request.getParameter("yearSearch");
				String monthSeach = request.getParameter("monthSeach");
				String yearSearchForMonth = request.getParameter("yearSearchForMonth");
				strJsonResp =  reAlertManagement.getLocationAlertHistory(connection.connection(),userId,Integer.parseInt(selectCane),yearSearch,monthSeach,yearSearchForMonth);
			}else  if ("reportChooseLocationAlert".equals(method)){
				String selectCane = request.getParameter("selectCane");
				String searchStartDate = request.getParameter("searchStartDate");
				String searchEndDate = request.getParameter("searchEndDate");
				strJsonResp =  reAlertManagement.getChooseLocationAlertHistory(connection.connection(),Integer.parseInt(selectCane),userId,searchStartDate,searchEndDate);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				connection.connection().close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		response.setCharacterEncoding("UTF8");
		response.getWriter().print(strJsonResp);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
