package servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import bus.InsertLocationAreaManagement;
import connection.connectionMysql;

@WebServlet("/InsertLocationAreaServlet")
public class InsertLocationAreaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
      
	connectionMysql connection = new connectionMysql();
    public InsertLocationAreaServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String method = request.getParameter("method");
		String strJsonResp = "";
		try {
			System.out.println("method : "+method);
			InsertLocationAreaManagement inAreaManagement = new InsertLocationAreaManagement();
			if ("loadSmartCaneRadius".equals(method)) {
				String selectSmartCane = request.getParameter("selectCane");
				strJsonResp =  inAreaManagement.getSmartCaneRadius(connection.connection(),Integer.parseInt(selectSmartCane));
			}else {
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				connection.connection().close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		response.setCharacterEncoding("UTF8");
		response.getWriter().print(strJsonResp);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String method = request.getParameter("method");
		HashMap<String, Object> respData = new HashMap<String, Object>();
		Gson gson = new Gson();
		String strJsonResp = "";
		System.out.println(method + " : method");
		try {
			InsertLocationAreaManagement areaManagement = new InsertLocationAreaManagement();
			if ("insertRadius".equals(method)) {
				String selectCane = request.getParameter("selectCane");
				String latitude = request.getParameter("latitude");
				String longitude = request.getParameter("longitude");
				String radiusArea = request.getParameter("radiusArea");
				String checkBox = request.getParameter("checkBox");
				respData.put("numRow", areaManagement.insertLocationArea(connection.connection(), selectCane, longitude, latitude, radiusArea,checkBox));
			}else {
				System.out.println("Not Method Post");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				connection.connection().close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		strJsonResp = gson.toJson(respData);
		response.setCharacterEncoding("UTF8");
		response.getWriter().print(strJsonResp);
	}

}
