package servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import bus.InsertUltrasonicManagement;
import connection.connectionMysql;

@WebServlet("/InsertUltrasonicServlet")
public class InsertUltrasonicServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	connectionMysql connection = new connectionMysql();    

    public InsertUltrasonicServlet() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String method = request.getParameter("method");
		HashMap<String, Object> respData = new HashMap<String, Object>();
		Gson gson = new Gson();
		String strJsonResp = "";
		System.out.println(method + " : method");
		try {
			InsertUltrasonicManagement iManagement = new InsertUltrasonicManagement();
			if ("insertUltrasonic".equals(method)) {
				String distanceUltrasonic = request.getParameter("distanceUltrasonic");
				String caneToken = request.getParameter("caneToken");
				if (Integer.parseInt(distanceUltrasonic)<=50 ) {
					iManagement.insertUltrasonic(connection.connection(), caneToken, distanceUltrasonic);
				}
				
			}else {
				System.out.println("Not Method Post");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				connection.connection().close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		strJsonResp = gson.toJson(respData);
		response.setCharacterEncoding("UTF8");
		response.getWriter().print(strJsonResp);
	}

}
