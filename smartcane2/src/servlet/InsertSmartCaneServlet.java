package servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import bean.UserConectionCaneBean;
import bus.InsertSmartCaneManagement;
import connection.connectionMysql;

@WebServlet("/InsertSmartCaneServlet")
public class InsertSmartCaneServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	connectionMysql connection = new connectionMysql();    
    public InsertSmartCaneServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String method = request.getParameter("method");
		HttpSession session = request.getSession();
		String userID = (String)session.getAttribute("userId");
		String strJsonResp = "";
		System.out.println("userID : "+userID);
		System.out.println("method : "+method);
		try {
			InsertSmartCaneManagement insertSmartCaneManagement = new InsertSmartCaneManagement();
			if ("loadSmartCane".equals(method)) {
				strJsonResp =  insertSmartCaneManagement.getSmartCaneName(connection.connection(),Integer.parseInt(userID));
			}else if ("loadSmartCaneInfo".equals(method)) {
				String userCaneId = request.getParameter("userCaneId");
				strJsonResp =  insertSmartCaneManagement.getSmartCaneInfo(connection.connection(),Integer.parseInt(userCaneId));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				connection.connection().close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		response.setCharacterEncoding("UTF8");
		response.getWriter().print(strJsonResp);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String userID = (String)session.getAttribute("userId");
		String method = request.getParameter("method");
		HashMap<String, Object> respData = new HashMap<String, Object>();
		Gson gson = new Gson();
		String strJsonResp = "";
		System.out.println(method + " : method");
		try {
			InsertSmartCaneManagement insertSmartCaneManagement = new InsertSmartCaneManagement();
			if ("addNewSmartCane".equals(method)) {
				String jsonNewSmartCane = request.getParameter("newSmartCane");
				UserConectionCaneBean[] arraySmartCane = gson.fromJson(jsonNewSmartCane,UserConectionCaneBean[].class);
				respData.put("numRow", insertSmartCaneManagement.insertCane(connection.connection(),Integer.parseInt(userID),arraySmartCane));
			}else if ("stopCane".equals(method)) {
				String userCaneId = request.getParameter("userCaneId");
				respData.put("numRow", insertSmartCaneManagement.updateCane(connection.connection(),userCaneId));
			}else if ("startCane".equals(method)) {
				String userCaneId = request.getParameter("userCaneId");
				respData.put("numRow", insertSmartCaneManagement.updateStartCane(connection.connection(),userCaneId));
			}else if ("addInfoCane".equals(method)) {
				String userCaneId = request.getParameter("userCaneId");
				String age = request.getParameter("age");
				String height = request.getParameter("height");
				String weight = request.getParameter("weight");
				String caneName = request.getParameter("caneName");
				respData.put("numRow", insertSmartCaneManagement.addInfoCane(connection.connection(),userCaneId,age,height,weight,caneName));
			}else {
				System.out.println("Not Method Post");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				connection.connection().close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		strJsonResp = gson.toJson(respData);
		response.setCharacterEncoding("UTF8");
		response.getWriter().print(strJsonResp);
	}

}
