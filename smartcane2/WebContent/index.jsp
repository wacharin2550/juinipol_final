<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1">    
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

<title>Login</title>
<style>
body{
background: darksalmon;
}
</style>
</head>
<body>

  
	<div class="container"> 
    <div class="row login-tab" style="margin: auto; padding-top: 25%;" >
    	<div class="col-md-4 "></div>
    	<div class="col-md-4 col-md-offset-4" >
    		<div class="card">
			  	<div class="card-header"  >
			    	<h3 class="card-title">Login</h3>
			 	</div>
			  	<div class="card-body">
			    	  	<div class="form-group">
			    		    <input class="form-control" id="username" type="text">
			    		</div>
			    		<div class="form-group">
			    			<input class="form-control" id="password"  type="password" value="">
			    		</div>
				    	
				    	<button type="button" class="btn btn-success" onclick="login();">Login</button>
			    </div>
			    <div class="card-footer">
			    	  <a href="#" id="registerButton">สมัครสมาชิก</a>
			    </div>
			</div>
		</div>
			<div class="col-md-4 "></div>
	</div>
	 <div class="row register-tab" style="margin: auto; padding-top: 20%;display: none;">
    	<div class="col-md-4 "></div>
    	<div class="col-md-4 col-md-offset-4" >
    		<div class="card">
			  	<div class="card-header"  >
			    	<h3 class="card-title">Register</h3>
			 	</div>
			  	<div class="card-body">
			  	 <span style="color: red;">***กรุณากรอกให้ครบทุกช่อง</span>
			  			<div class="form-group">
			    		    <input class="form-control" id="name" type="text" placeholder='ชื่อ'>
			    		</div>
			    		<div class="form-group">
			    		    <input class="form-control" id="lastname" type="text" placeholder='นามสกุล'>
			    		</div>
			    	  	<div class="form-group">
			    		    <input class="form-control" id="username_register" type="text" placeholder='ยูสเซอร์'>
			    		</div>
			    		<div class="form-group">
			    			<input class="form-control" id="password_register"  type="password" value="" placeholder='รหัสผ่าน'>
			    		</div>	
			    		<div class="form-group">
			    			<input class="form-control" id="password_register_retyp"  type="password" value="" placeholder='รหัสผ่านอีกครั้ง'>
			    		</div>
			    		<div style="float: right;">
			    		<button type="button" class="btn btn-success" onclick="register();" id='register_button'>สมัคร</button>
			 			<button type="button" class="btn btn-danger" id='cancel_button'>ยกเลิก</button>
			    		</div>
				    	
			    </div>
			</div>
		</div>
			<div class="col-md-4 "></div>
	</div>
</div>

	<script
  src="https://code.jquery.com/jquery-3.3.1.js"
  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
  crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	<script type="text/javascript" src="index.js"></script>
	<script type="text/javascript" src="plugin/sha256/sha256.js"></script>

</body>
</html>