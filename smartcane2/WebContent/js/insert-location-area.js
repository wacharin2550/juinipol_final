var marker;
var labels = 'สถานที่ของคุณ';
var labelIndex = 0;
var map;
var infowindow;
var placeMakerEiei = 0;
var markers = [];

$(document).ready(function() {
	loadSmartCane();
	setTimeout(() => {
		loadRadius();
	}, 2000);
	google.maps.event.addDomListener(window, "load", initMap);
});

var citymap = {
	nontaburi : {
		center : {
			lat : 13.920496651641498,
			lng : 100.55438279847544
		}
	}
};
function addMarker(location, map) {
	var marker = new google.maps.Marker({
		position : location,
		label : labels,
		map : map
	});
	markers.push(marker);
}
function setMapOnAll(map) {
    for (var i = 0; i < markers.length; i++) {
      markers[i].setMap(map);
    }
  }

function initMap(lat,lang,radius) {

	if (lat&&lang&&radius) {
		var silpakornMuangthong = {
				lat : lat,
				lng : lang
			};
	}else {
		var silpakornMuangthong = {
				lat : 13.914986177432098,
				lng : 100.55191367025816
			};
	}
	
	// Create the map.
	map = new google.maps.Map(document.getElementById('map'), {
		zoom : 16,
		center : silpakornMuangthong,
		mapTypeId : 'terrain'
	});
	if (lat&&lang&&radius) {
		setMapOnAll(null);
		for ( var city in citymap) {
			var cityCircle = new google.maps.Circle({
				strokeColor : '#FF0000',
				strokeOpacity : 0.8,
				strokeWeight : 2,
				fillColor : '#FF0000',
				fillOpacity : 0.35,
				map : map,
				center : {
					lat : lat,
					lng : lang
				},
				radius : radius
			});
		}
	addMarker(silpakornMuangthong, map);
	}else {
		
		for ( var city in citymap) {
			var cityCircle = new google.maps.Circle({
				strokeColor : '#FF0000',
				strokeOpacity : 0.8,
				strokeWeight : 2,
				fillColor : '#FF0000',
				fillOpacity : 0.35,
				map : map,
				center : {
					lat : 13.914986177432098,
					lng : 100.55191367025816
				},
				radius : 100
			});
		}
		setMapOnAll(null);
		 cityCircle.setMap(null);
	}
		

	google.maps.event.addListener(map, 'click', function(event) {
		 var myLatLng = event.latLng;
		 var lat = myLatLng.lat();
		 var lng = myLatLng.lng();
		 parseFloat($('#latitude').val(lat));
		 parseFloat($('#longitude').val(lng));
		 placeMarker(event.latLng);
		 setMapOnAll(null);
		 cityCircle.setMap(null);
	});
	
	var input = document.getElementById('pac-input');
	var searchBox = new google.maps.places.SearchBox(input);
//	map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

	// Bias the SearchBox results towards current map's viewport.
	map.addListener('bounds_changed', function() {
		searchBox.setBounds(map.getBounds());
	});



	searchBox.addListener('places_changed', function() {
		var places = searchBox.getPlaces();

		if (places.length == 0) {
			return;
		}

		markers.forEach(function(marker) {
			marker.setMap(null);
		});
		markers = [];

		var bounds = new google.maps.LatLngBounds();
		places.forEach(function(place) {
			if (!place.geometry) {
				console.log("Returned place contains no geometry");
				return;
			}
			var icon = {
				url : place.icon,
				size : new google.maps.Size(71, 71),
				origin : new google.maps.Point(0, 0),
				anchor : new google.maps.Point(17, 34),
				scaledSize : new google.maps.Size(25, 25)
			};

			markers.push(new google.maps.Marker({
				map : map,
				icon : icon,
				title : place.name,
				position : place.geometry.location
			}));

			if (place.geometry.viewport) {
				bounds.union(place.geometry.viewport);
			} else {
				bounds.extend(place.geometry.location);
			}
		});
		map.fitBounds(bounds);
	});
	
}

function placeMarker(location) {
	if (!marker || !marker.setPosition) {
		marker = new google.maps.Marker({
			position : location,
			map : map,
		});
	} else {
		marker.setPosition(location);
	}
	if (!!infowindow && !!infowindow.close) {
		infowindow.close();
	}
	infowindow = new google.maps.InfoWindow({
		content : 'Latitude: ' + location.lat() + '<br>Longitude: '
				+ location.lng()
	});
	infowindow.open(map, marker);
}

function loadSmartCane() {
	$.get("../InsertSmartCaneServlet", {
		method : "loadSmartCane"
	}).done(
			function(data) {
				var obj = $.parseJSON(data);
				var runningNo = 1;
				for (var i = 0; i < obj.length; i++) {
					if (obj[i].caneStatus == 1) {
						$("#selectSmartCane").append(new Option(obj[i].caneName, obj[i].caneUserId))
					}
				}
			}).fail(function() {
		alert("error");
	});
}

function showAreaRadius() {
	var latitude = parseFloat($('#latitude').val());
	var longitude = parseFloat($('#longitude').val());
	var radiusArea = parseFloat($('#radiusArea').val());
	initMap(latitude,longitude,radiusArea);
}
function createAreaRadius() {
	var selectCane = $('#selectSmartCane').val();
	var latitude = parseFloat($('#latitude').val());
	var longitude = parseFloat($('#longitude').val());
	var radiusArea = parseFloat($('#radiusArea').val());
	var checkBox = $( '#exampleCheck1' ).prop( "checked" );
	
	if ($.isNumeric(latitude) && $.isNumeric(longitude)&& $.isNumeric(radiusArea)) {
		$.post("../InsertLocationAreaServlet", {
			method : "insertRadius",
			selectCane : selectCane,
			latitude : latitude,
			longitude : longitude,
			radiusArea : radiusArea,
			checkBox:checkBox
		}).done(function(data) {
			var obj = $.parseJSON(data);
			if (obj.numRow) {
				alert("Success");
			}else {
				alert("Error");
			}
		}).fail(function(data) {
			alert("Error");
		});
	}

}

$( "#selectSmartCane" ).change(function() {
	var selectCane = $('#selectSmartCane').val();
	$.get("../InsertLocationAreaServlet", {
		method : "loadSmartCaneRadius",
		selectCane : selectCane
	}).done(
			function(data) {
				var obj = $.parseJSON(data);
				if (obj[0].locationStatus == 0) {
					$('#exampleCheck1').prop('checked', false);
				}else {
					$('#exampleCheck1').prop('checked', true);
				}
				if (obj.length>0) {
					$('#latitude').val(obj[0].latitude);
					$('#longitude').val(obj[0].longitude);
					$('#radiusArea').val(obj[0].radius);
					initMap(parseFloat(obj[0].latitude),parseFloat(obj[0].longitude),parseFloat(obj[0].radius));
				}if (obj.length==0) {
					$('#latitude').val("");
					$('#longitude').val("");
					$('#radiusArea').val("");
					$('#latitude').text("");
					$('#longitude').text("");
					$('#radiusArea').text("");
					initMap();
				}
			
			}).fail(function() {
		alert("error");
	});
});

function loadRadius() {
	var selectCane = $('#selectSmartCane').val();
	if (selectCane) {
		$.get("../InsertLocationAreaServlet", {
			method : "loadSmartCaneRadius",
			selectCane : selectCane
		}).done(
				function(data) {
					var obj = $.parseJSON(data);
					if (obj[0].locationStatus == 0) {
						$('#exampleCheck1').prop('checked', false);
					}else {
						$('#exampleCheck1').prop('checked', true);
					}
					if (obj.length>0) {
						$('#latitude').val(obj[0].latitude);
						$('#longitude').val(obj[0].longitude);
						$('#radiusArea').val(obj[0].radius);
						initMap(parseFloat(obj[0].latitude),parseFloat(obj[0].longitude),parseFloat(obj[0].radius));
					}if (obj.length==0) {
						$('#latitude').val("");
						$('#longitude').val("");
						$('#radiusArea').val("");
						$('#latitude').text("");
						$('#longitude').text("");
						$('#radiusArea').text("");
						initMap();
					}
				
				}).fail(function() {
			alert("error");
		});
	}

}