var dataDistance = []
var data;
var myChart

$(document).ready(
		function() {
			loadSmartCane();

			setTimeout(function() {
				loadUltrasonic();
			}, 1500);
			setTimeout(function() {
				var config = {
					type : 'pie',
					data : {
						datasets : [ {
							data : [ dataDistance[0], dataDistance[1],
									dataDistance[2], dataDistance[3] ],
							backgroundColor : [ window.chartColors.red,
									window.chartColors.orange,
									window.chartColors.yellow,
									window.chartColors.blue ],
							label : 'Dataset 1'
						} ],
						labels : [ 'มากกว่า 60  ซม.', 'มากกว่า 50  ซม.',
								'ระยะ 30-49 ซม.', 'ระยะ  29 ซม.' ]
					},
					options : {
						responsive : true
					}
				};
				var ctx = document.getElementById('chart-area')
						.getContext('2d');
				myChart = new Chart(ctx, config);
			}, 3000);
		});

function loadSmartCane() {
	$.get("../InsertSmartCaneServlet", {
		method : "loadSmartCane"
	}).done(
			function(data) {
				var obj = $.parseJSON(data);
				var runningNo = 1;
				for (var i = 0; i < obj.length; i++) {
					if (obj[i].caneStatus == 1) {
						$("#selectSmartCane").append(new Option(obj[i].caneName, obj[i].caneId))
					}
				}
			}).fail(function() {
		alert("error");
	});

}

function loadUltrasonic() {
	var selectCane = $('#selectSmartCane').val();
	var distanceMoreThanSixty = [];
	var distanceMoreThanFive = [];
	var distanceMoreThanThree = [];
	var distanceTwentyNine = [];
	if (selectCane) {
		$.get("../ReportUltrasonicServlet", {
			method : "reportUltrasonic",
			selectCane : selectCane
		}).done(
				function(data) {
					var obj = $.parseJSON(data);
					console.log(obj);
					if (obj.length > 0) {
						for (var i = 0; i < obj.length; i++) {
							if (obj[i].distanceMoreThanSixty) {
								distanceMoreThanSixty
										.push(obj[i].distanceMoreThanSixty);
							}
							if (obj[i].distanceMoreThanFive) {
								distanceMoreThanFive
										.push(obj[i].distanceMoreThanFive);
							}
							if (obj[i].distanceMoreThanThree) {
								distanceMoreThanThree
										.push(obj[i].distanceMoreThanThree);
							}
							if (obj[i].distanceTwentyNine) {
								distanceTwentyNine
										.push(obj[i].distanceTwentyNine);
							}
						}
						
						distanceForSelect = [
								distanceMoreThanSixty.length,
								distanceMoreThanFive.length,
								distanceMoreThanThree.length,
								distanceTwentyNine.length ];
						$('#sixty').text(distanceForSelect[0] + " ครั้ง");
						$('#fivety').text(distanceForSelect[1] + " ครั้ง");
						$('#three').text(distanceForSelect[2] + " ครั้ง");
						$('#two').text(distanceForSelect[3] + " ครั้ง");
						
						var config = {
							type : 'pie',
							data : {
								datasets : [ {
									data : [
											distanceForSelect[0],
											distanceForSelect[1],
											distanceForSelect[2],
											distanceForSelect[3] ],
									backgroundColor : [
											window.chartColors.red,
											window.chartColors.orange,
											window.chartColors.yellow,
											window.chartColors.blue ],
									label : 'Dataset 1'
								} ],
								labels : [
										'มากกว่า 60  ซม.',
										'มากกว่า 50  ซม.',
										'ระยะ 30-49 ซม.',
										'ระยะ  29 ซม.' ]
							},
							options : {
								responsive : true
							}
						};
						var ctx = document.getElementById('chart-area').getContext('2d');
						myChart = new Chart(ctx, config);
						
					} else {
						var config = {
							type : 'pie',
							data : {
								datasets : [ {
									data : [ 1, 1, 1, 1 ],
									backgroundColor : [
											window.chartColors.red,
											window.chartColors.orange,
											window.chartColors.yellow,
											window.chartColors.blue ],
									label : 'Dataset 1'
								} ],
								labels : [
										'มากกว่า 60  ซม.',
										'มากกว่า 50  ซม.',
										'ระยะ 30-49 ซม.',
										'ระยะ  29 ซม.' ]
							},
							options : {
								responsive : true
							}
						};
						var ctx = document
								.getElementById(
										'chart-area')
								.getContext('2d');
						myChart = new Chart(ctx, config);
						$('#sixty').text(0+ " ครั้ง");
						$('#fivety').text(0 + " ครั้ง");
						$('#three').text(0+ " ครั้ง");
						$('#two').text(0 + " ครั้ง");
					}
					
				}).fail(function() {
			alert("error");
		});
	}

}

$("#selectSmartCane").change(function() {
					var distanceMoreThanSixty = [];
					var distanceMoreThanFive = [];
					var distanceMoreThanThree = [];
					var distanceTwentyNine = [];
					var distanceForSelect = [];
					var selectCane = $('#selectSmartCane').val();
					if (selectCane) {
						$.get("../ReportUltrasonicServlet", {
									method : "reportUltrasonic",
									selectCane : selectCane
								}).done(function(data) {
											var obj = $.parseJSON(data);
											if (obj.length > 0) {
												for (var i = 0; i < obj.length; i++) {
													if (obj[i].distanceMoreThanSixty) {
														distanceMoreThanSixty
																.push(obj[i].distanceMoreThanSixty);
													}
													if (obj[i].distanceMoreThanFive) {
														distanceMoreThanFive
																.push(obj[i].distanceMoreThanFive);
													}
													if (obj[i].distanceMoreThanThree) {
														distanceMoreThanThree
																.push(obj[i].distanceMoreThanThree);
													}
													if (obj[i].distanceTwentyNine) {
														distanceTwentyNine
																.push(obj[i].distanceTwentyNine);
													}
												}
												
												distanceForSelect = [
														distanceMoreThanSixty.length,
														distanceMoreThanFive.length,
														distanceMoreThanThree.length,
														distanceTwentyNine.length ];
												$('#sixty').text(distanceForSelect[0] + " ครั้ง");
												$('#fivety').text(distanceForSelect[1] + " ครั้ง");
												$('#three').text(distanceForSelect[2] + " ครั้ง");
												$('#two').text(distanceForSelect[3] + " ครั้ง");
												
												myChart.destroy();
												
												var config = {
													type : 'pie',
													data : {
														datasets : [ {
															data : [
																	distanceForSelect[0],
																	distanceForSelect[1],
																	distanceForSelect[2],
																	distanceForSelect[3] ],
															backgroundColor : [
																	window.chartColors.red,
																	window.chartColors.orange,
																	window.chartColors.yellow,
																	window.chartColors.blue ],
															label : 'Dataset 1'
														} ],
														labels : [
																'มากกว่า 60  ซม.',
																'มากกว่า 50  ซม.',
																'ระยะ 30-49 ซม.',
																'ระยะ  29 ซม.' ]
													},
													options : {
														responsive : true
													}
												};
												var ctx = document.getElementById('chart-area').getContext('2d');
												myChart = new Chart(ctx, config);
												
											} else {
												var config = {
													type : 'pie',
													data : {
														datasets : [ {
															data : [ 1, 1, 1, 1 ],
															backgroundColor : [
																	window.chartColors.red,
																	window.chartColors.orange,
																	window.chartColors.yellow,
																	window.chartColors.blue ],
															label : 'Dataset 1'
														} ],
														labels : [
																'มากกว่า 60  ซม.',
																'มากกว่า 50  ซม.',
																'ระยะ 30-49 ซม.',
																'ระยะ  29 ซม.' ]
													},
													options : {
														responsive : true
													}
												};
												var ctx = document
														.getElementById(
																'chart-area')
														.getContext('2d');
												myChart = new Chart(ctx, config);
												$('#sixty').text(0+ " ครั้ง");
												$('#fivety').text(0 + " ครั้ง");
												$('#three').text(0+ " ครั้ง");
												$('#two').text(0 + " ครั้ง");
											}
											
										}).fail(function() {
									alert("error");
								});
					}
				

				});

var colorNames = Object.keys(window.chartColors);
