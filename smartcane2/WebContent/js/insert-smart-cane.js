var dtSmartCane
var hot;
$( document ).ready(function() {
	createSmartCane();
	loadSmartCane();
//	createSmartCanetHandsontable();
});



function createSmartCane() {
	dtSmartCane =$('#insert-smart-cane').DataTable({
		responsive: true
	});	
}

function loadSmartCane() {
	dtSmartCane.clear().draw();
	$.get( "../InsertSmartCaneServlet",{method:"loadSmartCane"}).done(function( data ) {
		var obj = $.parseJSON(data);	
		var stopButton;
		var editInfoButton;
		var runningNo = 1;
		for (var i = 0; i < obj.length; i++) {
			if (obj[i].caneStatus==1) {
				stopButton = '<button type="button" class="btn btn-danger" onclick="stopCane('+obj[i].caneUserId+');" ">หยุด</button>'
			}else {
				stopButton = '<button type="button" class="btn btn-success" onclick="startCane('+obj[i].caneUserId+');" ">เริ่มใช้งาน</button>'
			}
		
			editInfoButton = '<button type="button" class="btn btn-primary" onclick="addInfoCane('+obj[i].caneUserId+');" ">แก้ไขข้อมูลไม้เท้า</button>'

			dtSmartCane.row.add( [runningNo,obj[i].caneName,obj[i].tokenID,
				checkNull(obj[i].age),
				checkNull(obj[i].weight),
				checkNull(obj[i].height),
				editInfoButton,
				stopButton
			     ] ).draw();
			 runningNo++;
		}
		
	}).fail(function() {
	    alert( "error" );
	});
	
}
function checkNull(value) {
	if (value) {
		value = value;
	}else {
		value = "";
	}
	return value;
}

function showModalInsert() {
	$('#modalDialogS').modal('show');
	createSmartCanetHandsontable();
}

function createSmartCanetHandsontable(){
	var container = document.getElementById('newSmartCaneContainer');
	var colHeaders =['<span lang="departmentCode">CANE NAME</span>','<span lang="department">CANE TOKEN</span>'];
//	$('#show-when-new-department-not-completed').hide();
	$('#modalDialogS').on('shown.bs.modal', function () {
//		$('#show-when-new-department-not-completed').hide();
		hot = new Handsontable(container, {
		  data: [["",""]],
		  rowHeaders: true,
		  colHeaders: true,
		  height: 300,
		  colWidths:230,
		  allowInsertColumn: true,
		  renderAllColumns: true,
		  colHeaders:colHeaders,
		  contextMenu: ['row_above', 'row_below', '---------', 'remove_row', '---------', 'undo', 'redo', '---------', 'copy', 'cut'],
		  columns: [
		            {	    
		            	type:'text'

		            },{
		            	type:'text'
		            }
            ]
		});
	});
}

function insertNewSmartCaneToDB(){
	var rows = hot.getData();
	var isnull = false;
	var newSmartCane = [];
	rows.forEach(function (each) {		
		var obj = {
				smartCaneName: each[0].trim(),
				smartCaneToken: each[1].trim(),
		};
		newSmartCane.push(obj);
		console.log(newSmartCane);
		console.log(newSmartCane[0].smartCaneName);
		console.log(newSmartCane[0].smartCaneToken);
		if (newSmartCane[0].smartCaneName&&newSmartCane[0].smartCaneToken) {
			isnull = true;
		}else {
			isnull = false;
		}
	});
	if (isnull) {
		$.post("../InsertSmartCaneServlet", { method: "addNewSmartCane", newSmartCane: JSON.stringify(newSmartCane)} )
		.done(function(data) {
			var obj = $.parseJSON(data);	
			console.log(obj);
			if (obj.numRow.isSuccess) {
				$("#modalDialogS").modal("hide")
				loadSmartCane();
				hot.destroy();
			}else {
				alert("บันทึกข้อมูลผิดพลาด");
			}
				
		}).fail(function(err) {
			console.log(err);
		});
	}
	else {
		alert("อย่าให้ค่าว่าง");
	}

}

function stopCane(userCaneId){
	if (userCaneId) {
		$.post("../InsertSmartCaneServlet", { method: "stopCane", userCaneId: userCaneId} )
		.done(function(data) {
			var obj = $.parseJSON(data);	
			if (obj.numRow) {
				$("#modalDialogS").modal("hide")
				loadSmartCane();
			}else {
				console.log("Error");
			}
				
		}).fail(function(err) {
			console.log(err);
		});
	}
	else {
		alert("อย่าให้ค่าว่าง");
	}
}

function startCane(userCaneId){
	if (userCaneId) {
		$.post("../InsertSmartCaneServlet", { method: "startCane", userCaneId: userCaneId} )
		.done(function(data) {
			var obj = $.parseJSON(data);	
			if (obj.numRow) {
				$("#modalDialogS").modal("hide")
				loadSmartCane();
			}else {
				alert("Error");
			}
		}).fail(function(err) {
			console.log(err);
		});
	}
	else {
		alert("อย่าให้ค่าว่าง");
	}
}

function addInfoCane(userCaneId){
	$('#modalDialog-info').modal('show');
	$("#cane-user-id").val(userCaneId);
	
	if (userCaneId) {
		$.get( "../InsertSmartCaneServlet",{method:"loadSmartCaneInfo",userCaneId:userCaneId}).done(function( data ) {
			var obj = $.parseJSON(data);	
			$("#height").val(obj[0].height);
			$("#weight").val(obj[0].weight);
			$("#age").val(obj[0].age);
			$("#edit-smart-cane-name").val(obj[0].caneName);
		}).fail(function() {
		    alert( "error" );
		});
	}
	
}
function addInfoSmartCane(){
	var age = $("#age").val();
	var height = $("#height").val();
	var weight = $("#weight").val();
	var userCaneId = $("#cane-user-id").val();
	var caneName = $("#edit-smart-cane-name").val();
	if (userCaneId) { 
		$.post("../InsertSmartCaneServlet", { method: "addInfoCane",caneName:caneName, userCaneId: userCaneId,age:age,height:height,weight:weight} )
		.done(function(data) {
			var obj = $.parseJSON(data);	
			if (obj.numRow) {
				$("#modalDialog-info").modal("hide")
				loadSmartCane();
			}else {
				alert("Error");
			}
				
		}).fail(function(err) {
			console.log(err);
		});
	}
	else {
		alert("อย่าให้ค่าว่าง");
	}
}
