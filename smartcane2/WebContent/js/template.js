$( document ).ready(function() {
	checkSession();
});

function logout() {
	$.post( "../Test", { method:"logout"}).done(function( data ) {
		var obj = $.parseJSON(data);
		if (obj.status) {
			location.href = "../../smartcane2";
		}
	}).fail(function(data) {
		var obj = $.parseJSON(data);	
		console.log(obj);
	});
}

function checkSession() {
	$.get( "../Test",{method:"checkSession"}).done(function( data ) {
		var obj = $.parseJSON(data);	
		console.log(obj);
		if (obj.respStatusOnline) {
			$('#username-id').html("ชื่อ : "+obj.userNameID);
		}else {
			location.href = "../../smartcane2";
		}
	}).fail(function() {
	    alert( "error" );
	});
}