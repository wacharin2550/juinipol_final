<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Report Ultrasonic</title>
<%@ include file="template.jsp"%>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.js"></script>
<script src="../plugin/chartJs/utils.js"></script>
</head>
<body>
	<div class="container">
	<div style="padding: 5"></div><br />
		<h2>สร้างบริเวณไม้เท้า</h2>

		<div class="form-group">
			<label for="sel1">เลือกไม้เท้า</label> <select class="form-control"
				id="selectSmartCane" style="width: 20%">

			</select>
		</div>
		<span>มากกว่า 60 ซม. = </span><span id='sixty'></span><br /> <span>มากกว่า
			50 ซม. = </span><span id='fivety'></span><br /> <span>ระยะ 30-49 ซม.
			= </span><span id='three'></span><br /> <span>ระยะ 29 ซม. = </span><span
			id='two'></span>
		<div id="canvas-holder">
			<canvas id="chart-area"></canvas>
		</div>
	</div>



	<script src="../js/report-ultrasonic.js"></script>
</body>
</html>