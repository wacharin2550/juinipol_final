<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-UTF-8">
<title>สร้างบริเวณไม้เท้า</title>
<%@ include file="template.jsp"%>
<style>
.container{
	width: 100%;
	height: 100%;
	background: white;
	padding-bottom: 100%;
}

#map {
	margin-left: auto;
	margin-right: auto;
	width: 100%;
	height:100%;
	padding: 25%;
}
/* Optional: Makes the sample page fill the window. */
html, body {
	height: 100%;
	margin: 0;
	padding: 0;
}

#description {
	font-family: Roboto;
	font-size: 15px;
	font-weight: 300;
}

#infowindow-content .title {
	font-weight: bold;
}

#infowindow-content {
	display: none;
}

#map #infowindow-content {
	display: inline;
}

.pac-card {
	margin: 10px 10px 0 0;
	border-radius: 2px 0 0 2px;
	box-sizing: border-box;
	-moz-box-sizing: border-box;
	outline: none;
	box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
	background-color: #fff;
	font-family: Roboto;
}

#pac-container {
	padding-bottom: 12px;
	margin-right: 12px;
}

.pac-controls {
	display: inline-block;
	padding: 5px 11px;
}

.pac-controls label {
	font-family: Roboto;
	font-size: 13px;
	font-weight: 300;
}

#pac-input {
	background-color: #fff;
	font-family: Roboto;
	font-size: 15px;
	font-weight: 300;
	margin-left: 12px;
	padding: 0 11px 0 13px;
	text-overflow: ellipsis;
	width: 400px;
}

#pac-input:focus {
	border-color: #4d90fe;
}

#title {
	color: #fff;
	background-color: #4d90fe;
	font-size: 25px;
	font-weight: 500;
	padding: 6px 12px;
}

#target {
	width: 345px;
}

</style>
</head>
<body>
	<div class="container" style="margin-top: 20px;">
	<div style="padding: 5"></div><br />
		<h2>สร้างบริเวณไม้เท้า</h2>

		<div class="form-group">
			<label for="sel1">เลือกไม้เท้า</label> <select class="form-control"
				id="selectSmartCane" style="width: 20%">

			</select>
			<div style="padding: 10px;"></div>
			<div class="form-check">
				<input type="checkbox" class="form-check-input" id="exampleCheck1">
				<label class="form-check-label" for="exampleCheck1">
					***เชื่อมกับพื้นที่เพื่อให้แจ้งเตือนเมื่ออกนอกบริเวณ</label>
			</div>
			<div style="padding: 10px;"></div>
			<div class="row">
				<div class="col-sm-4">
					<input class="form-control" id="latitude" type="text"
						placeholder='ละติจูด'>
				</div>
				<div class="col-sm-4">
					<input class="form-control" id="longitude" type="text"
						placeholder='ลองติจูด'>
				</div>
				<div class="col-sm-2">
					<input class="form-control" id="radiusArea" type="text"
						placeholder='รัศมีกรอบ'>
				</div>
				<div class="col-sm-2">
					<button type="button" class="btn btn-primary" id='cancel_button'
						onclick="showAreaRadius();">แสดง</button>
					<button type="button" class="btn btn-success" id='cancel_button'
						onclick="createAreaRadius();">บันทึก</button>
				</div>
				<div style="padding: 20px;"></div>

				<div class="col-sm-12">
				
					<h2>ค้นหาสถานที่ของคุณ</h2>
						<input id="pac-input" class="form-control" type="text"
							placeholder="หาสถานที่ของคุณ">
								<div style="padding: 5"></div><br />
					<div id="map"></div>
				</div>
			</div>
		</div>
	</div>


	<script type="text/javascript" src="../js/insert-location-area.js"></script>
	<script
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD8G9vmTJl3OsJP9CqoW9QMCbGweJA473s&libraries=places"></script>

</body>
</html>