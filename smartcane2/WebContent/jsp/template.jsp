<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1">
<link rel="stylesheet"
	href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=Kanit" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
<title>template</title>
<style>
body {
	font-family: 'Kanit', sans-serif;
	background:#DCDCDC;
}
.tab-content {
	background: white;
}

.container .nav-link {
	color: black;
	background: white;
}

.nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
	color: #fff;
	background: #007bff;
}
</style>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01"
		aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarTogglerDemo01">
		<a class="navbar-brand" href="#">SmartCane</a>
		<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
			<li class="nav-item"><a class="nav-link"
				href="insert_location_area.jsp">สร้างบริเวณไม้เท้า<span
					class="sr-only">(current)</span></a></li>
			<li class="nav-item"><a class="nav-link" href="current_gps.jsp">ตรวจสอบตำแหน่งไม้เท้า</a>
			</li>

			<li class="nav-item dropdown"><a
				class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false"> ลงทะเบียน </a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item" href="insert_line_cane.jsp">เชื่อมไม้เท้ากับไลน์</a>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="insert_smart_cane.jsp">ลงทะเบียนไม้เท้า</a>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="insert_line.jsp">ลงทะเบียนไลน์</a>
				</div></li>

			<li class="nav-item dropdown"><a
				class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false"> ดูรายงาน </a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item" href="report_healthy.jsp">รายงานสุขภาพ</a>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="report_location_alert.jsp">รายงานเมื่อออกนอกบริเวณ</a>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="report_emergency.jsp">รายงานกดปุ่มฉุกเฉิน</a>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="report_gps.jsp">รายงานการเดินทาง</a>
<!-- 				<div class="dropdown-divider"></div> -->
<!-- 					<a class="dropdown-item" href="report_ultrasonic.jsp">รายงานอัลตราโซนิค</a> -->
				</div></li>
		</ul>
		<span id="username-id" style="margin-right: 50px;color: white">username</span>
		<button class="btn btn-danger my-2 my-sm-0"
			onclick="logout();">ออกจากระบบ</button>
	</div>
	</nav>
	
	<script src="https://code.jquery.com/jquery-3.3.1.js"
		integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
		integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
		integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
		crossorigin="anonymous"></script>
				<script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.8.1/parsley.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script type="text/javascript" src="../js/template.js"></script>

</body>
</html>