
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ลงทะเบียนไม้เท้า</title>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
<link  data-jsfiddle="common" rel="stylesheet" media="screen" href="../plugin/handsontable/handsontable.full.min.css">
<%@ include file="template.jsp" %>
<style>
.container{
	width: 100%;
	height: 100%;
	background: white;
}
#modalDialogS{
		z-index: 80;
} 
.modal-backdrop {
		z-index: 60;
}
#newSmartCaneContainer{
		z-index: 90;
} 
.htContextMenu {
	z-index: 100;
}

</style>

</head>
<body>
<div class="container" style="margin-top: 20px;"><div style="padding: 5"></div>
<br />
<button onclick="showModalInsert();"  class="btn btn-info">ลงทะเบียน</button><div style="padding: 5"></div>
<br />
	<div class="card card-primary">
		<table id="insert-smart-cane"	class="table table-dynamic table-tools">
			<thead>
				<tr>
					<th >ลำดับ</th>
					<th >ชื่ออุปกรณ์</th>		
					<th >Token ไม้เท้า</th>
					<th >อายุ</th>
					<th >น้ำหนัก</th>
					<th >ส่วนสูง</th>
					<th >แก้ไขข้อมูลไม้เท้า</th>
					<th>ยกเลิก</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>
	
	<div class="modal fade" id="modalDialogS" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id=modalDialogS-title >ลงทะเบียนไม้เท้า</h4>
				</div>
				<div class="modal-body" id="modalDialogS-body" >
					<div id="newSmartCaneContainer"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success" onclick="insertNewSmartCaneToDB();">บันทึก</button>
					<button type="button" id="modalDialogS-closeBt"	class="btn btn-primary" data-dismiss="modal" lang="close">Close</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modalDialog-info" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id=modalDialogS-title >แก้ไขข้อมูลไม้เท้า</h4>
				</div>
				<div class="modal-body" id="modalDialogS-body" >
				 <input type="hidden" id="cane-user-id">
					<div class="form-group">
					<br />
						 <label for="edit-smart-cane-name">ชื่อไม้เท้า  :</label>
						 <input type="text" class="form-control" id="edit-smart-cane-name" placeholder="กรอกชื่อไม้เท้า">
						 <br />
						 <label for="age">อายุ  :</label>
						 <input type="text" class="form-control" id="age" placeholder="กรอกอายุ">
						 <br />
						  <label for="height">ส่วนสูง  :</label>
						 <input type="text" class="form-control" id="height"  placeholder="กรอกส่วนสูง">
						 	 <br />
						 	  <label for="weight">น้ำหนัก  :</label>
						  <input type="text" class="form-control" id="weight"  placeholder="กรอกน้ำหนัก">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success" onclick="addInfoSmartCane();">บันทึก</button>
					<button type="button" id="modalDialogS-closeBt"	class="btn btn-primary" data-dismiss="modal" lang="close">Close</button>
				</div>
			</div>
		</div>
	</div>
		</div>
<script data-jsfiddle="common" src="../plugin/handsontable/handsontable.full.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
<script type="text/javascript" src="../js/insert-smart-cane.js"></script>
</body>
</html>